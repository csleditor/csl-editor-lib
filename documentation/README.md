# About the Project

This Project is a fork of the cslEditor (see [https://github.com/citation-style-language/csl-editor-demo-site](https://github.com/citation-style-language/csl-editor-demo-site) and [https://github.com/citation-style-language/csl-editor](https://github.com/citation-style-language/csl-editor)). It extends the implementation of an editor to adjust CSL-Code. CSL-Code is used to describe citation formats for various well known reference management software. For more Information on CSL visit [https://citationstyles.org/](https://citationstyles.org/). In the original editor the following functionality was implemented. A Search for Styles, based on their names (searchByName), and an interactive search based on the closest match to your example (searchByExample). A Visual Editor as well as A Code Editor was implemented. The Code Editor lets you edit the CSL-Code itself, which is xml-based. The Visual Editor lets you edit the underlying CSL-Code in a tree based form. You can edit nodes, introduce case differentiation and more. We added an additional functionality to this editor, called Law Editor. Therefore this documentation has the focus on this new part, the Law Editor.

## Law Editor

The LawEditor consists of to parts. The upper half houses the exampleCitations. You can change these citations by selecting the drop-down menu on the upper right. The example Citations are generated on base of the underlying csl-data. The lower half houses several sections. In the code these sections are sometimes called sourceSections or dndSections. For every Citation Source / Type, which is important for the German law, a section is shown. You can edit the citation format for this type by changing the order of the fields. You may add attributes by dragging them from the right to the left as well as delimiters and freetexts. The upper left fieldset defines the format in the first occurrence of a citation, the second one defines further occurrences, whereas the bottom one defines the appearance in the bibliography. You can add customized delimiters and freetexts by typing into the input fields. Hovering over a field shows a toolbar to edit the format of this attribute/freetexts.
 
# Structure of the Project

This project is structured into three repositories. The first one is the csl-law-editor. This repository encloses the front end for the Website. It produces the structure of the website and loads the logic and the computational parts. These parts are in a second Repository cslEditorlib, which is the main repository. cslEditorlib is integrated into the first repository as submodule. The third repository generates necessary Data for the Law Editor (translationData). It is integrated with a submodule under external/translationData. 

## cslEditorLib
As mentioned, this is the main repository. We will only look at the newly implemented part concerning the LawEditor. Therefore you may start with the module {@link module:CSLEDIT_LawEditor}. It handles (or references to)  all the functionality which is implemented inside the LawEditor. You may also have a look at {@link module:CSLEDIT_LawViewController}. It is called whenever the underlying data is changed. You can add callbacks for the views, to trigger actions whenever the data changes. The data itself is represented by {@link module:CSLEDIT_data}. It will support you with a json representation of the XML-file and some helpful functions. PLease look at the Source Code of this file. Before the LawEditor is loaded, a {@link module:CSLEDIT_preProcessor} is called. The preProcessor performs some adjusting to the CSL-data. These changes are necessary, so the LawEditor can process the data. In fact the lawEditor itself only supports parts of the CSL-Specification, which is why the Data needs to be converted to the supported part before editing. Converting this data does not reduces functionality.

The LawEditor functionality is split into different parts. First the example Citation generation, which is handled by {@link module:CSLEDIT_citationEngine.runCiteprocAndDisplayOutput}. Secondly the sections with the different types of Sources. This Part is handled by {@link module:CSLEDIT_sourceSections}. The drag and Drop functionality is implemented in {@link module:CSLEDIT_dndFunctionality}. Furthermore, editing the style (i.e. bold,italic,...) of a field is implemented in {@link module:CSLEDIT_styleFormatting} which loads the {@link module: CSLEDIT_AttributesToolbar}. The Attribute Toolbar is the little PopUp by hovering over these fields.

## Translation Data

This repository contains files and functionality which describes the mapping between [CSL-Variables](https://docs.citationstyles.org/en/stable/specification.html?highlight=cs%3Ainfo#appendix-iv-variables) and their meaning in the German law. Not every field, which is supported by csl has a clear meaning in the German law. In addition reference managers like Zotero add their own meaning to the CSL-Variable. For that reason, the translationData supports a mapping in form of a csv-file, to define the meaning of csl-variables in the German law. We also added a python script to generate example citations from this mapping as well as the file which defines the sections in the LawEditor ({@link module:CSLEDIT_dummyRawData}).

# How to contribute 

If you like to contribute to this project, you should clone [csleditor/csl-law-editor](https://gitlab.com/csleditor/csl-law-editor) including all submodules. 
Please have a look at the [issues](https://gitlab.com/groups/csleditor/-/issues), to see what needs to be done.

Beside this documentation it might be helpful to have a look at the [documentation of the old project](https://github.com/citation-style-language/csl-editor/wiki). Please keep in mind that not everything described in there is possible due to changes. 
Also a documentation of the project in German is available [here](../../documentation/Dokumentation.pdf). In this documentation the design and decision process ist described in more detail, as well as the problems that have come up, implementing this.
  

## Prerequisites
  You first need to install the necessary packages: 
  The installation was tested on a fresh Ubuntu installation (18.04 LTS), but should also work with other systems.
  
  - Clone the Project with ``git clone git@gitlab.com:matcos/csl-law-editor.git --recursive``
  - Install Jekyll with Ruby Gem: (taken from <https://jekyllrb.com/docs/installation/ubuntu/>). You will find instructions for other systems [here](https://jekyllrb.com/docs/installation/).
  - ``sudo apt-get install ruby-full build-essential zlib1g-dev``
  - ``echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc``
  - ``echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc``
  - ``echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc``
  - ``source ~/.bashrc ``
  - ``gem install jekyll bundler``
  
## Build and Serve
  
  You can now build the Editor with the command ``bundle exec jekyll build``. If you like to watch the generated Website in your brower type instead of ``build`` the command  ``serve``. It is pretty heplful to give also the option ``--watch``, so jekyll automatically updates whenever files changed.
  
   - To view the served site, point your browser to <http://127.0.0.1:4000/csl-editor/>. 
   - To run the unit Tests,  point your browser to <http://127.0.0.1:4000/csl-editor/cslEditorLib/pages/unitTests.html>. 

## Contribute

  Unfortunately the Project is not finished yet. Please have a look at the [issues](https://gitlab.com/groups/csleditor/-/issues) for minor and major improvements. The issues can be categorized in the following way. In addition to this, the issues are labeled with ``TODO`` for necessary improvement and ``enhancement`` for improvements that are not necessary but would increase the usability.

### PreProcessor

The PreProcessor is really slow and needs to much Memory. It is in fact too slow to be used properly. In Addition it produces CSL-Code which contains redundant and duplicated code. This code can be rectified. Also the LawEditor doesn't support affixes (prefixes and suffixes). This is a design decision to keep the editor slim and easy. For that reason the PreProcessor needs to convert affixes into delimiters or freeTexts (depending on whether one should be able to format them). Moreover there is a bug in citeproc-js, which leads to misinterpreting delimiters as suffixes. One possible solution would be to encapsulate all variables in to macros. This way, the bug does not appear. Other solutions would be possible too.

To prevent crashes casued by the memory usage, you can preload a file to which the preprocessor has already been applied. Such a file is provided [here](../../documentation/neue-juristische-wochenschrift_angepasst.csl)
### LawEditor

Some of the planned functionality of the LawEditor is not implemented yet. So far the LawEditor only supports editing the CSL-Data by executing format styles on variables and freeText. The DragnDrop functionality as well as the delete and copyFromFirst Buttons have no effect on the CSL-Data and only change the view. This is the major issue which needs to be implemented. You will find additional information as well as pseudo code in [this file](../../documentation/laweditorchanges.pdf). Further some minor improvements can be done. It would be good to only show the citations in example citations which are important for the opened section. Also at this point it is not possible to detect names. Names are a separate class of CSL variables with more possibilities for adjustment and editing. Since these features can be edited by changing to the Visual Editor it is only labeled as ``enhancement``. Also the Editor does not support the class of terms. Here it would be necessary to do investigations into the way terms work and are used, since it is not properly specified by the CSL-Specification. 

### General

Some issues concern the behavior whole Editor. Mostly these are enhancement which would improve the usability. It would be nice if the LawEditor would open by clicking edit on a style. Also it would be great to support a general template for law styles which is standardly loaded at the beginning. For the Law-Editor it was implemented to display not only the first occurrence but also further occurrences of citations. This is an important feature for the German law. It might be good to add these to the Code- and Visual Editor as well, so it improves the usability of the "old" parts for lawyer or law students. In fact the old display of the example citations are distorted since it ignores the fact that a second occurrence can be different to the first one.
  
  
## Documentation
  Please add jsdoc comments to your code and describe what is done in each function. 
  For information about jsdoc please visit their [documentation](http://usejsdoc.org/index.html), especially where it is explained [how to comment files loaded with an AMD module loader](http://usejsdoc.org/howto-amd-modules.html). It also may help you to have a look at existing commented files.
  
### Generate this Documentation
   First, you need to install [ink-docstrap](https://github.com/docstrap/docstrap) for the used template:
   `` npm install ink-docstrap ``
   
   Run from your Home Directory the following command. Replace PathToTheRepo with the path to the cloned Repository. If you only have cloned the csEditorLib, replace the path up to this point.
    
   ``jsdoc -c PathToTheRepo/editor_repo/cslEditorLib/documentation/config.json -r PathToTheRepo/editor_repo/cslEditorLib/src/ -d PathToTheRepo/editor_repo/cslEditorLib/documentation/ -R  PathToTheRepo/editor_repo/cslEditorLib/documentation/README.md``
   
   It may be possible to run jsdoc directly in the documentation folder, so that you don't need to give the whole path for every parameter. Unfortunately in some cases npm is not able to find the necessary node modules from this path.
   Please consider that this installation has worked an an Ubuntu 18.04, but may be different on other systems. Please visit their Websites ([nodejs](https://nodejs.org/en/), [docstrap](https://github.com/docstrap/docstrap)) for troubleshooting


