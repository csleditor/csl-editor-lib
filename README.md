# cslEditorLib

This Project is a fork of the cslEditor (see [https://github.com/citation-style-language/csl-editor](https://github.com/citation-style-language/csl-editor)). It extends the implementation of an editor to adjust CSL-Code. CSL-Code is used to describe citation formats for various well known reference management software. For more Information on CSL visit [https://citationstyles.org/](https://citationstyles.org/). In the original editor the following functionality was implemented. A Search for Styles, based on their names (searchByName), and an interactive search based on the closest match to your example (searchByExample). A Visual Editor as well as A Code Editor was implemented. The Code Editor lets you edit the CSL-Code itself, which is xml-based. The Visual Editor lets you edit the underlying CSL-Code in a tree based form. You can edit nodes, introduce case differentiation and more. We added an additional functionality to this editor, called Law Editor.

Play with the reference implementation here: [Citation Style Editor](https://csleditor.gitlab.io/csl-law-editor/about/).

This project is structured into three repositories. The first one is the csl-law-editor. This repository encloses the front end for the Website. It produces the structure of the website and loads the logic and the computational parts. These parts are in a second Repository cslEditorlib, which is this repository. cslEditorlib is integrated into the first repository as submodule. The third repository generates necessary Data for the Law Editor (translationData). It is integrated with a submodule under external/translationData. 

## Contribute

1. Follow instructions to set up the reference implementation [here](https://gitlab.com/csleditor/csl-law-editor).
2. See the [developer documentation](https://csleditor.gitlab.io/csl-law-editor/documentation/) for further documentation.

## Attributions 

- [Citation Style Language](http://citationstyles.org/)
- [CSL style repository](https://github.com/citation-style-language/styles)
- [citeproc-js](http://gsl-nagoya-u.net/http/pub/citeproc-doc.html) (Citation formatting engine)
- [CodeMirror](http://codemirror.net/) (text editor on codeEditor page)
- [diff\_match\_patch](http://code.google.com/p/google-diff-match-patch/) (for calculating edit distances in Search by Example)
- [Trang](http://www.thaiopensource.com/relaxng/trang.html) (for converting schema files from .rnc to .rng)
- [node.js](http://node.js.org) (for javascript run in the build process)
- [FamFamFam Silk icons](http://www.famfamfam.com/lab/icons/silk/)
- [Fugue icons](http://p.yusukekamiyamane.com/)
- [jQuery](http://jquery.com/)
- [jQuery UI](https://jqueryui.com/)
- [jQuery jsTree Plugin](http://www.jstree.com/) (tree view on visualEditor page)
- [jQuery UI Layout Plugin](http://layout.jquery-dev.net)
- [jQuery hoverIntent Plugin](http://cherne.net/brian/resources/jquery.hoverIntent.html)
- [jQuery scrollTo Plugin](http://demos.flesler.com/jquery/scrollTo/)
- [QUnit](http://qunitjs.com/) (javascript unit testing framework)
- [RequireJS](http://requirejs.org/) (javascript module loader)
- [mustache](http://mustache.github.com/) (used to generate HTML from templates)
- [pagedown](http://code.google.com/p/pagedown/) (used to generate source code docmentation)
- [XRegExp](http://xregexp.com/) (for unicode letter matching in the Search by Example page)
- [FontAwesome](https://fontawesome.com/)

