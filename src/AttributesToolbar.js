"use strict";

/**
 * A Toolbox / Menu for editing the styles for the Attribute
 * Modified Version of {@link module:CSLEDIT_richTextToolbar}
 * @module CSLEDIT_AttributesToolbar
 * @author Matthias Cosler
 */
define([],function (){

            /**
             * Creates a AttributeToolbar
             *
             *  - call {@link module:CSLEDIT_AttributesToolbar.initToolbar}
             *  - adds all Buttons to the Toolbar using {@link module:CSLEDIT_AttributesToolbar.addButton}
             *  - adds handler to the buttons {@link module:CSLEDIT_AttributesToolbar.updateButtonState}
             *
             * @class
             * @alias module:CSLEDIT_AttributesToolbar
             * @param {* | jQuery} container a jQuery element in which the toolbar should appear
             * @param {array} options a list of options, which should be displayed. every option can be enabled or disabled.
             * @example <caption> Example for options</caption>
             *      [{option: "bold", enabled : true}, ...]
             * @param {function} callback a function to call whenever a button is clicked. Takes a parameter, which is the $button and a second, which are the known options
             * @param {array} knownOptions contains several informations about the formatting style (italic, bold etc)
             * @constructor
             * @author Matthias Cosler
             */
			var CSLEDIT_AttributesToolbar = function (container, options, knownOptions, callback) {


                var toolbarElement;
                var buttons = [];
                var currentCallback = null;

                $(document).ready(function () {
                    toolbarElement = $('<div class="toolbar richText has-arrow">');
                    toolbarElement.append('<span class="pointer">');

                    /**
                     * Adds a Button to this toolbar
                     *
                     * @param {string} style the style the button should have
                     * @param {string} title the title the button will have
                     * @param {string} innerHTML the Text which is dispalyed inside the button
                     * @returns {*|jQuery} the button itself
                     * @alias module:CSLEDIT_AttributesToolbar.addButton
                     * @author Matthias Cosler
                     * @public
                     */
                    var addButton = function (style, title, innerHTML) {
                        var button = $('<a>')
                            .attr('href', '#')
                            .attr('data-style', style)
                            .attr('title', title)
                            .append(innerHTML);

                        buttons.push(button);
                        toolbarElement.append(button);
                        return button;
                    };


                    $.each(options, function (index, item) {
                        var found = false;
                        $.each(knownOptions, function (searchIndex, searchItem) {
                           if (searchItem.style === item.option){
                               found = true;
                               var button = addButton(searchItem.style, searchItem.title, searchItem.html);
                               if (item.enabled) {
                                   button.addClass("selected");
                               }
                               return false;
                           }
                        });
                        if (found === false){
                            throw new Error(item + " has no template in toolbar");
                        }
                    });


                    toolbarElement.find('a').click(function (event) {
                        var $target = $(event.currentTarget);

                        updateButtonState($target);

                        if (currentCallback !== null) {
                            currentCallback($target, knownOptions);
                        }

                        event.preventDefault();
                    });

                    toolbarElement.css({
                        "display": "inline-block",
                        "overflow": "visible",
                        "position": "absolute"
                    });

                });

                /**
                 * Initializes the Toolbar, hence produce the elements inside the container the toolbar needs.
                 *
                 * Enables the mousenter and mouseleave event ans sets necessary css code
                 *
                 * @alias module:CSLEDIT_AttributesToolbar.initToolbar
                 * @author Matthias Cosler
                 * @public
                 */
                var initToolbar = function () {
                    var innerContainer = $('<div/>');
                    innerContainer.append(container.html());
                    container.html("");
                    container.append(innerContainer);
                    toolbarElement.css({
                        "display": "inline-block"
                        //"bottom": -5
                    });
                    container.append(toolbarElement);
                    $(toolbarElement).fadeOut(0);

                    container.mouseenter(function () {
                        showToolbar(container, callback);
                    });

                    container.mouseleave(function () {
                        hideToolbar();
                    });
                };

                /**
                 * will deactivate all buttons which are in the same group as button
                 *
                 *  - gets the group number of the clicked button
                 *  - iterate through all buttons and deactivates buttons in the same group
                 *  - triggers the callback function for each deactivated button
                 *
                 * @param {object} button the clicked button
                 * @alias module:CSLEDIT_AttributesToolbar.triggerDependencies
                 * @author Matthias Cosler
                 * @public
                 */
                var triggerDependencies = function (button) {
                    //get group number of clicked item
                    var searchStyle = button.attr("data-style");
                    var searchGroupNumber;
                    $.each(knownOptions, function (optionIndex, option) {
                        if (option.style === searchStyle ){
                            searchGroupNumber = option.group;
                        }
                    });

                    //go through all buttons
                    $.each(buttons, function (index, item) {
                        var searchStyle = item.attr("data-style");
                        //search corresponding option
                        $.each(knownOptions, function (optionIndex, option) {
                            if (option.style === searchStyle && option.group === searchGroupNumber){
                                if (item.hasClass("selected")) {
                                    item.removeClass("selected");
                                    if (currentCallback !== null) {
                                        currentCallback($(item), knownOptions);
                                    }
                                }
                                return false;
                            }
                        });
                    });
                };


                /**
                 * Is called whenever a button is clicked.
                 *
                 * - set selected or unset selected, depending on the state before
                 * - if set selected, also trigger dependencies {@link module:CSLEDIT_AttributesToolbar.triggerDependencies}
                 *
                 * @param {object} button the button which was clicked
                 * @alias module:CSLEDIT_AttributesToolbar.updateButtonState
                 * @author Matthias Cosler
                 * @public
                 */
                var updateButtonState = function (button) {
                    if (button.hasClass("selected")) {
                        button.removeClass("selected");
                    } else {
                        triggerDependencies(button);
                        button.addClass("selected");
                    }
                };

                /**
                 * Hides the Toolbar and Resets the callback
                 *
                 * @alias module:CSLEDIT_AttributesToolbar.hideToolbar
                 * @public
                 */
                var hideToolbar = function () {
                    $(toolbarElement).fadeOut(0);
                    currentCallback = null;
                };

                /**
                 * Shows the toolbar and enables the callback
                 *
                 * @alias module:CSLEDIT_AttributesToolbar.showToolbar
                 * @public
                 */
                var showToolbar = function () {
                    currentCallback = callback;
                    $(toolbarElement).fadeIn(0);
                };

                initToolbar();

            };




	return CSLEDIT_AttributesToolbar;
});
