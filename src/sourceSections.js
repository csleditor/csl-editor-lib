"use strict";

/**
 * Contains Functionality to fill the drag'n'Drop Sections
 * @module CSLEDIT_sourceSections
 * @see module:CSLEDIT_styleFormatting
 * @see module:CSLEDIT_initData
 * @see module:CSLEDIT_dndFunctionality
 * @author Matthias Cosler
 */
define(
    [
        'src/styleFormatting',
        'src/citationTypeInitData',
        "src/dndFunctionality"
    ],
    function (
        CSLEDIT_styleFormatting,
        CSLEDIT_initData,
        CSLEDIT_dndFunctionality
    ) {


        /**
         * Initialises the clone.
         *
         * This function is called whenever a filed is duplicated i.e. cloned
         *  - if element was attribute or FreeText
         *      - remove the toolbar from clone since it is inked through jquery to the wrong element
         *      - clone the Toolbar using {@link module:CSLEDIT_styleFormatting.cloneToolbar}
         *  - if input field is cloned
         *      - copy the value of hte old input filed into the new one
         *
         * @param {object} clone. The object to initialize
         * @param {object} orig the original Element
         * @alias module:CSLEDIT_sourceSections.initClones
         * @private
         * @author Matthias Cosler
         */
        function initClones(clone, orig) {
            var origToolbar = orig.children().first().children().last();
            //init toolbar
            if (typeof $(clone).children("div.AttributeContent")[0] !== 'undefined') {
                $(clone).find("div.toolbar")[0].remove();
                CSLEDIT_styleFormatting.cloneToolbar(origToolbar ,$($(clone).children("div.AttributeContent")[0]));
            } else if (typeof $(clone).children("div.FreeTextContent")[0] !== 'undefined') {
                $(clone).find("div.toolbar")[0].remove();
                CSLEDIT_styleFormatting.cloneToolbar(origToolbar ,$($(clone).children("div.FreeTextContent")[0]));
            }

            //if text input -> copy value
            if ($(clone).find("input").length) {
                $(clone).find("input").val($(orig).find("input").val());
            }
        }

        /**
         * Populates the lists (i.e. first occurence, further Occurence, bibliograhy, attributes, delimiters, freetext)
         *
         * This function fills all the elements into the given list
         *  - iterates through data
         *  - extracts the type of the element (attribute or delimiter or freetext)
         *  - adds the item of data as json to the html object
         *  - extracting styleOptions using {@link module:CSLEDIT_styleFormatting.getOptionsFromItem}
         *  - add Zotero name to object as title if attribute
         *  - if attribute r freetext, add Toolbar using {@link module:CSLEDIT_styleFormatting.addToolbar} with the extracted styleOptions
         *  - applying the extracted styleOptions on the html element using {@link module:CSLEDIT_styleFormatting.applyFormats}
         *
         * @param {object} data the data wich is used to fill the list
         * @param {object} container a refernecce to the list which is to be filled
         * @param {number} sectionIndex the sectionIndex to get alternatve names
         * @alias module:CSLEDIT_sourceSections.fillList
         * @private
         * @example <caption> Example for data </caption>
            [{
                "styleList": ["I"],
                "cslIdList": [["1213","group"],["106","macro"],["795","macro"],["796","group"],["798","date"]],
                "value": "Datum Veröffentlichung",
                "type": "attribute",
                "variableType": "issued",
                "currentID": 798
            }, ... ]
         @author Matthias Cosler
         */
        var fillList = function (data, container, sectionIndex) {
            $.each(data, function (index, item) {
                var $elementcontainer = $(document.createElement("div"));
                if (item.type === "delimiter"){
                    $elementcontainer = $elementcontainer.addClass("DelimiterContent");
                } else if (item.type === "attribute") {
                    $elementcontainer = $elementcontainer.addClass("AttributeContent");
                } else if (item.type === "freeText"){
                    $elementcontainer = $elementcontainer.addClass("FreeTextContent");
                }
                $(container).append($(document.createElement("li")).addClass("ui-state-default").append($elementcontainer));
                var obj = JSON.stringify(item);
                $($elementcontainer).attr("obj", obj );
                $($elementcontainer).text(item.value);

                var options = CSLEDIT_styleFormatting.getOptionsFromItem(item);

                if (item.type === "attribute"){
                    //if attribute
                    if (item.value === ""){
                        item.value = CSLEDIT_initData.getLawName(item.variableType, sectionIndex);
                    }
                    $($elementcontainer).text(item.value);

                    $($elementcontainer).attr("title", CSLEDIT_initData.getZoteroName(item.variableType, sectionIndex) ); //TODO something which is visible

                    CSLEDIT_styleFormatting.addToolbar($($elementcontainer), options);
                } else if (item.type === "freeText"){
                    //if freeText

                    CSLEDIT_styleFormatting.addToolbar($($elementcontainer), options);
                }

                CSLEDIT_styleFormatting.applyFormats($($elementcontainer), options);


            });
        };

        /**
         * Empties the Field element
         *
         * This fuction will be added to the handler by clicking on the button emptyFieöd (see {@link module:CSLEDIT_sourceSections.addDeleteButtonsToFieldSets}
         *
         * @param {object} element the element which is to be emptied
         * @alias module:CSLEDIT_sourceSections.emptyField
         * @private
         * @author Matthias Cosler
         */
        var emptyField = function (element) {
            $(element).parent().siblings(".AttributesDNDTarget").empty();
        };

        /**
         * Copies from the first (firstOcurrence) into the field element
         *
         * This fuction will be added to the handler by clicking on the button CopyFromFirst (see {@link module:CSLEDIT_sourceSections.addCopyButtonsToFieldSets}
         *
         * @param {object} element
         * @alias module:CSLEDIT_sourceSections.copyFromFirst
         * @private
         * @author Matthias Cosler
         */
        var copyFromFirst = function (element) {
            var copyTo = $(element).parent().siblings(".AttributesDNDTarget");
            copyTo.empty();

            var firstOcc = $(element).parent().parent().siblings(".FirstOccurrenceFieldset").children(".AttributesDNDTarget"); //Traverse to the first occurrence
            $(firstOcc).find("li").each(function(){
                var node = $(this).clone();
                //console.log(node);
                copyTo.append(node);
                initClones(node, $(this));
            });
        };

        /**
         * Generates the frames where the lists will be filled in.
         *
         * This function builds the framework, for all the sections, and lists
         *  - Adds the section Name to the Sections
         *  - Add the container for the fieldsets
         *  - Adds the fieldsets, namely firstOccFieldset, furtherOccFieldset, bibFieldset, attrFieldset, delFieldset, textFieldset
         *  - Adds legends to all fieldsets
         *
         * @param {(*|jQuery)} $container the element in which the sectins will be generated
         * @param {number} index the index of the section we are filling
         * @param {object} section the section which is to be filled
         * @alias module:CSLEDIT_sourceSections.generateFrames
         * @private
         * @returns {{firstOccFieldset: (*|jQuery), furtherOccFieldset: (*|jQuery), attrFieldset: (*|jQuery), textFieldset: (*|jQuery), bibFieldset: (*|jQuery), delFieldset: (*|jQuery)}} references to the fields which were generated
         * @author Matthias Cosler
         */
        var generateFrames = function ($container, index, section) {

            // append header
            var head = $(document.createElement("h3")).attr("id", section.sectionTitle + "Head")
                .text(CSLEDIT_initData.getLawName("type", index))
                .attr("title", "Zotero: " + CSLEDIT_initData.getZoteroName("type", index));

            /*var head = $(document.createElement("h3")).attr("id", section.sectionTitle + "Head")
                .text(CSLEDIT_initData.getLawName(section.sectionTitle),index); TODO somehow display also zotero name in section title*/
            $container.append(head);



            //append container (header, content, left, right)
            var headcontainer = $(document.createElement("div")).attr("id", section.sectionTitle);
            $container.append(headcontainer);
            var contentContainer = $(document.createElement("div")).addClass("OptionContentContainer");
            $(headcontainer).append(contentContainer);
            var leftContainer = $(document.createElement("div")).addClass("OptionContentLeft");
            var rightContainer = $(document.createElement("div")).addClass("OptionContentRight");
            $(contentContainer).append(leftContainer);
            $(contentContainer).append(rightContainer);

            //append Fieldsets (first, further Occurence, bibliography
            var firstOccFieldset = $(document.createElement("fieldset"))
                .addClass("OptionsFieldset")
                .addClass("FirstOccurrenceFieldset");
            var furtherOccFieldset = $(document.createElement("fieldset"))
                .addClass("OptionsFieldset")
                .addClass("FurtherOccurrenceFieldset");
            var bibFieldset = $(document.createElement("fieldset"))
                .addClass("OptionsFieldset")
                .addClass("BibliographyFieldset");
            $(leftContainer).append(firstOccFieldset);
            $(leftContainer).append(furtherOccFieldset);
            $(leftContainer).append(bibFieldset);

            //Append Fieldsets (attributes, delimiter, freeText)
            var attrFieldset = $(document.createElement("fieldset"))
                .addClass("OptionsFieldset");
            var delFieldset = $(document.createElement("fieldset"))
                .addClass("OptionsFieldset");
            var textFieldset = $(document.createElement("fieldset"))
                .addClass("OptionsFieldset");
            $(rightContainer).append(attrFieldset);
            $(rightContainer).append(delFieldset);
            $(rightContainer).append(textFieldset);

            //add legends to fieldsets
            $(firstOccFieldset).append($(document.createElement("legend"))
                .text("Erstes Auftreten (Fußnote)"));
            $(furtherOccFieldset).append($(document.createElement("legend"))
                .text("Weitere Auftreten (Fußnote)"));
            $(bibFieldset).append($(document.createElement("legend"))
                .text("Literaturverzeichnis"));
            $(attrFieldset).append($(document.createElement("legend"))
                .text("Atttribute"));
            $(delFieldset).append($(document.createElement("legend"))
                .text("Trennzeichen"));
            $(textFieldset).append($(document.createElement("legend"))
                .text("Freitext"));

            return {
                firstOccFieldset : firstOccFieldset,
                furtherOccFieldset : furtherOccFieldset,
                bibFieldset : bibFieldset,
                attrFieldset : attrFieldset,
                delFieldset : delFieldset,
                textFieldset : textFieldset
            };
        };

        /**
         * Adds the empty lists to the fieldsets
         *
         * Takes all fieldsets and adds EmptyLists to them. The empty lists will contain all the DragnDrop Elements.
         *
         * @param {object} section the section
         * @param {(*|jQuery)} fieldsets the fieldsets
         * @alias module:CSLEDIT_sourceSections.appendEmptyLists
         * @private
         * @returns {{bibList: (*|jQuery), textList: (*|jQuery), firstList: (*|jQuery), delList: (*|jQuery), attrList: (*|jQuery), furtherList: (*|jQuery)}} a reference to all the lists which were generated
         * @author Matthias Cosler
         */
        var appendEmptyLists = function (section, fieldsets) {

            var firstList = $(document.createElement("ul"))
                .attr("id", section.sectionTitle + "FirstOccurence")
                .addClass("AttributesDND")
                .addClass("AttributesDNDTarget");

            var furtherList = $(document.createElement("ul"))
                .attr("id", section.sectionTitle + "FurtherOccurence")
                .addClass("AttributesDND")
                .addClass("AttributesDNDTarget");

            var bibList = $(document.createElement("ul"))
                .attr("id", section.sectionTitle + "Bibliography")
                .addClass("AttributesDND")
                .addClass("AttributesDNDTarget");

            var attrList = $(document.createElement("ul"))
                .attr("id", section.sectionTitle + "Attributes")
                .addClass("AttributesDND")
                .addClass("AttributesDNDSource");

            var delList = $(document.createElement("ul"))
                .attr("id", section.sectionTitle + "Delimiters")
                .addClass("DelimitersDND");

            var textList = $(document.createElement("ul"))
                .attr("id", section.sectionTitle + "FreeTexts")
                .addClass("FreeTextsDND");


            $(fieldsets.firstOccFieldset).append(firstList);
            $(fieldsets.furtherOccFieldset).append(furtherList);
            $(fieldsets.bibFieldset).append(bibList);
            $(fieldsets.attrFieldset).append(attrList);
            $(fieldsets.delFieldset).append(delList);
            $(fieldsets.textFieldset).append(textList);

            return {
                firstList : firstList,
                furtherList : furtherList,
                bibList : bibList,
                attrList : attrList,
                delList : delList,
                textList : textList
            };
        };

        /**
         * Adds the delete buttons to all fieldsets
         *
         * This function will add the delete Button to all fieldsets on the left side. The delte Button empties the List inside the Fieldset
         *
         * @param {(*|jQuery)} fieldsets the fieldsets the buttons are appended to
         * @alias module:CSLEDIT_sourceSections.addDeleteButtonsToFieldSets
         * @private
         * @author Matthias Cosler
         */
        var addDeleteButtonsToFieldSets = function (fieldsets) {
            var emptyFieldButton1 = $(document.createElement("i"))
                .addClass("fas")
                .addClass("fa-trash-alt")
                .attr("title", "Leere das Feld");
            $(fieldsets.firstOccFieldset).append($(document.createElement("div"))
                .addClass("emptyField")
                .append(emptyFieldButton1));
            $(emptyFieldButton1).click(function () {
                emptyField(emptyFieldButton1);
            });


            var emptyFieldButton2 = $(document.createElement("i"))
                .addClass("fas")
                .addClass("fa-trash-alt")
                .attr("title", "Leere das Feld");

            $(fieldsets.furtherOccFieldset).append($(document.createElement("div"))
                .addClass("emptyField")
                .append(emptyFieldButton2));
            $(emptyFieldButton2).click(function () {
                emptyField(emptyFieldButton2);
            });

            var emptyFieldButton3 = $(document.createElement("i"))
                .addClass("fas")
                .addClass("fa-trash-alt")
                .attr("title", "Leere das Feld");
            $(fieldsets.bibFieldset).append($(document.createElement("div"))
                .addClass("emptyField")
                .append(emptyFieldButton3));
            $(emptyFieldButton3).click(function () {
                emptyField(emptyFieldButton3);
            });

        };

        /**
         * Adds the copy from first buttons th the second and third fieldset
         *
         * This function will add the CopyFromFirstButton th the second and third fieldste on the left. The CopyFromFirstButton will duplicate the content form the first fieldset into the clicked one.
         *
         * @param {(*|jQuery)} fieldsets the fieldsets, the buttona are appended to
         * @alias module:CSLEDIT_sourceSections.addCopyButtonsToFieldSets
         * @private
         * @author Matthias Cosler
         */
        var addCopyButtonsToFieldSets = function (fieldsets) {
            var copyFromFirstButton1 = $(document.createElement("i"))
                .addClass("fas")
                .addClass("fa-copy")
                .attr("title", "Kopiere aus erstem Auftreten");
            $(fieldsets.furtherOccFieldset).append($(document.createElement("div"))
                .addClass("emptyField")
                .append(copyFromFirstButton1));
            $(copyFromFirstButton1).click(function () {
                copyFromFirst(copyFromFirstButton1);
            });


            var copyFromFirstButton2 = $(document.createElement("i"))
                .addClass("fas")
                .addClass("fa-copy")
                .attr("title", "Kopiere aus erstem Auftreten");
            $(fieldsets.bibFieldset).append($(document.createElement("div"))
                .addClass("emptyField")
                .append(copyFromFirstButton2));
            $(copyFromFirstButton2).click(function () {
                copyFromFirst(copyFromFirstButton2);
            });

        };

        /**
         * Adds the input fields to delList and freeList
         *
         * The delimiterList and the FreeTextList will have an input Field to allow the user to amend the options. These inputFields are added here.
         *
         * @param {(*|jQuery)} lists the list where the input fields are added to
         * @alias module:CSLEDIT_sourceSections.addInputFields
         * @private
         * @author Matthias Cosler
         */
        var addInputFields = function (lists) {
            //add FreeTextDelimiter
            var elementcontainerDel = $(document.createElement("div"));
            elementcontainerDel.addClass("DelimiterContent");
            $(lists.delList).append($(document.createElement("li")).addClass("ui-state-default").append(elementcontainerDel));
            var objDel = JSON.stringify({"cslIdList" : [] , "type": "delimiter", "styleList": [], "value": "input"});
            $(elementcontainerDel).attr("obj", objDel );
            $(elementcontainerDel).append('<input class="DelimiterContent" type="text" placeholder="schreiben...">');
            //TODO inputfield needs callback which changes the JSON

            //add FreeText
            var elementcontainer = $(document.createElement("div"));
            elementcontainer.addClass("FreeTextContent");
            $(lists.textList).append($(document.createElement("li")).addClass("ui-state-default").append(elementcontainer));
            var obj = JSON.stringify({"cslIdList" : [] , "type": "freeText", "styleList": [], "value": "input"});
            $(elementcontainer).attr("obj", obj );
            $(elementcontainer).append('<input class="FreeTextContent" type="text" placeholder="schreiben...">');
            CSLEDIT_styleFormatting.addToolbar($(elementcontainer));
            //TODO inputfield needs callback which changes the JSON
        };


        /**
         * This will populate the sourceSections using the Init Data
         *
         * This is the function which populates all the Content in the sections
         *  - Iterate through the initData (see {@link module:CSLEDIT_initData.getData})
         *  - Populate each section by calling
         *      - {@link module:CSLEDIT_sourceSections.generateFrames}
         *      - {@link module:CSLEDIT_sourceSections.appendEmptyLists}
         *      - {@link module:CSLEDIT_sourceSections.addDeleteButtonsToFieldSets}
         *      - {@link module:CSLEDIT_sourceSections.addCopyButtonsToFieldSets}
         *      - {@link module:CSLEDIT_sourceSections.fillList} on formattedCitations, formattedFurtherCitations, formattedBibiography from the parsed Data (see {@link module:CSLEDIT_citationEngine.runCiteprocAndParseOutput})
         *      - {@link module:CSLEDIT_sourceSections.fillList} on attributeStack,, delimiterStack, freeTexStack from the collected and default data (see {@link module:CSLEDIT_initData.getSectionData})
         *      - {@link module:CSLEDIT_sourceSections.addInputFields} to finally add the inputfields to the necessary fieldsets
         *
         * @param {(*|jQuery)} $container the element in which the content will be filled
         * @alias module:CSLEDIT_sourceSections.fillContent
         * @private
         * @author Matthias Cosler
         */
        var fillContent = function ($container) {
            //TODO better, if content too large weird stuff happens
            //TODO make textfield better draggable
            var initData = CSLEDIT_initData.getData();

            $.each(initData, function (index, element) {

                var fieldsets = generateFrames($container, index, element);
                var lists = appendEmptyLists(element, fieldsets);
                addDeleteButtonsToFieldSets(fieldsets);
                addCopyButtonsToFieldSets(fieldsets);

                //Fill firstOccurence
                fillList(initData[index].citationFields.formattedCitations, lists.firstList, index);

                //Fill furtherOccurence
                fillList(initData[index].citationFields.formattedFurtherCitations, lists.furtherList, index);

                //Fill Bibliography
                fillList(initData[index].citationFields.formattedBibliography, lists.bibList, index);

                //Fill the attributes
                fillList(initData[index].attributeStack, lists.attrList, index);

                //Fill Delimiters
                fillList(initData[index].delimiterStack, lists.delList, index);

                //Fill FreeTexts
                fillList(initData[index].freeTextStack, lists.textList, index);

                addInputFields(lists);

            });

        };

        /**
         * Will remove all  dynamic content from the DragNDrop Fields
         *
         * @alias module:CSLEDIT_sourceSections.removeContent
         * @private
         * @todo not implemented
         */
        var removeContent = function ($container) {
            //$container.empty();
            //TODO
        };

        /**
         * will reload all dynamic content in SourceSections
         *
         * @alias module:CSLEDIT_sourceSections.reloadContent
         * @public
         * @todo not implemented
         */
        var reloadContent = function () {
            //TODO this does not work since jquery UI breaks
            /*var $container = $("div#OptionContent");
            removeContent($container);
            fillContent($container);
            $(document).ready(function () {
                //needds to pass initClones function ass argument, otherwise it is not recognised
                CSLEDIT_dndFunctionality.initDndFunctions(initClones);
            });*/
            window.location.reload();
        };

        /**
         * will reload all dynamic content in SourceSections
         *
         *  - identifiy the container in whic the content will be populated
         *  - call {@link module:CSLEDIT_sourceSections.fillContent}
         *  - when the site is loaded, initialise the dndFuncionality using {@link module:CSLEDIT_dndFunctionality.initDndFunctions}
         *
         * @alias module:CSLEDIT_sourceSections.loadContent
         * @public
         * @author Matthias Cosler
         */
        var loadContent = function () {
            var $container = $("div#OptionContent");
            fillContent($container);
            $(document).ready(function () {
                //needds to pass initClones function ass argument, otherwise it is not recognised
                CSLEDIT_dndFunctionality.initDndFunctions(initClones);
            });
        };




        return {
            reloadContent : reloadContent,
            loadContent : loadContent
        };
    });



