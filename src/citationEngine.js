"use strict";

/**
 * Uses citeproc-js to generate example citaitons
 *
 * @module CSLEDIT_citationEngine
 * @see module:CSLEDIT_options
 * @see module:CSLEDIT_exampleCitations
 * @see module:CSLEDIT_diff
 * @see module:CSLEDIT_data
 * @see module:debug
 * @see module:citeprocSys
 * @see external/citeproc/citeproc
 */
define([	'src/options',
			'src/exampleCitations',
			'src/diff',
			'src/dataInstance',
			'src/debug',
			'src/citeprocLoadSys',
			'external/citeproc/citeproc',
			'jquery'
		],
		function (
			CSLEDIT_options,
			CSLEDIT_exampleCitations,
			CSLEDIT_diff,
			CSLEDIT_data,
			debug,
			citeprocSys,
			CSL,
			$
		) {
			var
				oldFormattedCitation = "",
				newFormattedCitation = "",
				oldFormattedFurtherCitation = "",
				newFormattedFurtherCitation = "",
				oldFormattedBibliography = "",
				newFormattedBibliography = "",
				diffTimeout,
				dmp = null, // for diff_match_patch object
				previousStyle = "", // to skip initializing citeproc when using the same style
				citeproc;

			/**
			 * Remove the tags with the given tagName from the given html and return the result
			 *
			 * The contents of the removed tags are retained
			 *
			 * @param html
			 * @param tagName
			 * @returns {string}
			 * @alias module:CSLEDIT_citationEngine.stripTags
			 * @private
			 */
			var stripTags = function (html, tagName) {
				var stripRegExp = new RegExp("<" + tagName + ".*?>|</\\s*" + tagName + "\\s*?>", "g");

				// creating new string because of bug where some html from generateExampleCitations.js
				// was type object instead of string and didn't have the replace() function
				var stripped = html.toString();
				stripped = stripped.replace(stripRegExp, "");
				return stripped;
			};

			/**
			 * Formats the given citationClusters, containing the given documents, in the given style
			 *
			 * If taggedOutput is true, the output will contain <span cslid=???> tags where the cslid attribute points to the input CSL node responsible for that part of the output
			 *
			 * Returns a result containing the following properties:
			 *
			 *  - statusMessage - used for errors, if everything went well, this will be an empty string
			 *  - formattedCitations - a list of formatted inline citation strings
			 *  - formattedBibliography - the formatted bibliography string
			 *
			 * @param style
			 * @param documents
			 * @param citationClusters
			 * @param taggedOutput
			 * @returns {{formattedFurtherCitations: Array, formattedBibliography: Array, formattedCitations: Array, statusMessage: string}}
			 * @alias module:CSLEDIT_citationEngine.formatCitations
			 * @author Matthias Cosler (furtherCitations)
			 * @public
			 */
			var formatCitations = function (style, documents, citationClusters, taggedOutput) {
				var bibliography,
					result,
					citations,
					furtherCitations,
					inLineCitations,
					inLineCitationArray,
					furtherInLineCitations,
					furtherInLineCitationArray,
					occurencesInLineArray,
					pos,
					makeBibliographyArgument,
					enumerateCitations;

				citeprocSys.setJsonDocuments(documents);
				citeprocSys.csl_reverse_lookup_support = true;

				result = { "statusMessage": "", "formattedFurtherCitations": [], "formattedCitations": [], "formattedBibliography": [] };
				result.statusMessage = "";
				if (style !== previousStyle) {
					try {
						citeproc = new CSL.Engine(citeprocSys, style);
						previousStyle = style;
					}
					catch (err) {
						result.statusMessage = "Citeproc initialisation exception: " + err;
						return result;
					}
				} else {
					citeproc.restoreProcessorState([]);
				}

				inLineCitations = "";
				inLineCitationArray = [];
				occurencesInLineArray = [];

				$.each(citationClusters, function (clusterIndex, cluster) {
					if (cluster.citationItems.length === 0) {
						return;
					}
					try {
						citations = citeproc.appendCitationCluster(cluster, false);
					}
					catch (err) {
						result.statusMessage = "Citeproc exception: " + err;
						return false;
					}

					$.each(citations, function (i, citation) {
						pos = citation[0];

						if (inLineCitations !== "")
						{
							inLineCitations += "<br>";
						}

						if (taggedOutput !== true) {
							citation[1] = stripTags(citation[1], "span");
						}
						inLineCitations += citation[1];

						if (citation[1] !== "") {
							inLineCitationArray.push(citation[1]);
							occurencesInLineArray.push(citation[0]);
						}
					});

				});


				furtherInLineCitations = "";
				furtherInLineCitationArray = [];

				$.each(citationClusters, function (clusterIndex, cluster) {
					if (cluster.citationItems.length === 0) {
						return;
					}
					try {
						furtherCitations = citeproc.appendCitationCluster(cluster, false);
					//    There is a very weird behaviour. In the first each block, every citationcluster is generated once.
					//	  in this each block, not only the new cluster is in the output, but all cluster before as well. But only the clusters "before" from this each block.
					// 	  The output is like this: 0 - 1 - 2 for the first block and
					//							   3 - 3 - 4 - 3 - 4 - 5 for the second block.
					//    It gets even more weird when a citation is two times in different clusters.
					//    Solution: Test if cluster is already in inlineCititionArray and push only if not.
					//	  This is a very ugly solution, but since there arent many example citations (and I'm out of Ideas) this should be okay.
					//    Possible Bug in  citeproc.appendCitationCluster?? -> Should construct minimal example reproducing the phenomenon -> not that important

					}
					catch (err) {
						result.statusMessage = "Citeproc exception: " + err;
						return false;
					}

					$.each(furtherCitations, function (i, citation) {
						pos = citation[0];

						if (furtherInLineCitations !== "")
						{
							furtherInLineCitations += "<br>";
						} else {
						}

						if (taggedOutput !== true) {
							citation[1] = stripTags(citation[1], "span");
						}
						furtherInLineCitations += citation[1];
						if (citation[1] !== "" && ($.inArray(citation[0],occurencesInLineArray)===-1)) {
							furtherInLineCitationArray.push(citation[1]);
							occurencesInLineArray.push(citation[0]);
						}
					});


				});
				if (result.statusMessage !== "") {
					return result;
				}
				result.formattedCitations = inLineCitationArray;
				result.formattedFurtherCitations = furtherInLineCitationArray;

				enumerateCitations = true;
				if (enumerateCitations === true) {
					makeBibliographyArgument = undefined;
				}
				else {
					makeBibliographyArgument = "citation-number";
				}

				try {
					bibliography = citeproc.makeBibliography(makeBibliographyArgument);
				}
				catch (err) {
					result.statusMessage = "Citeproc exception: " + err;
					return result;
				}

				if (bibliography !== false) {
					if ("hangingindent" in bibliography[0]) {
						result.hangingIndent = bibliography[0].hangingindent;
					}
					bibliography = bibliography[1];
				}
				else {
					bibliography = [[(citations[0][1])]];
				}

				if (taggedOutput !== true) {
					$.each(bibliography, function (i, entry) {
						bibliography[i] = stripTags(entry, "span");
					});
				}

				result.formattedBibliography = bibliography;
				return result;
			};

			/**
			 * This function formats the current style in CSLEDIT_data and populates the given elements with the output
			 *
			 * Note on diffs:
			 * There is currently unused code to show a diff for a second after each change.
			 * This can be enabled by adding 'showDiffOnChange' to the CSLEDIT_options via the CSLEDIT_VisualEditor or CSLEDIT_CodeEditor contructors.
			 * It hasn't worked so well since adding the reverse lookup <span cslid=??> tags to the citeproc output.
			 * Could be good to fix for use in the Code Editor, but not so essential for the Visual Editor.
			 *
			 * @param data
			 * @param statusOut
			 * @param citationsOut
			 * @param bibliographyOut
			 * @param callback
			 * @param exampleReferences
			 * @param exampleCitations
			 * @param furtherCitationsOut
			 * @alias module:CSLEDIT_citationEngine.runCiteprocAndDisplayOutput
			 * @public
			 */
			var runCiteprocAndDisplayOutput = function (
					data, statusOut, citationsOut, bibliographyOut, callback,
					exampleReferences, exampleCitations, furtherCitationsOut) {

				debug.time("runCiteprocAndDisplayOutput");

				var style = data.getCslCode(),
					inLineCitations = "",
					citations = [],
					formattedResult,
					citationTagStart = "",
					citationTagEnd = "",
					furtherCitationTagStart = "",
					furtherCitationTagEnd = "",
					bibliographyTagStart = "",
					bibliographyTagEnd = "",
					startTime,
					citationDiffs,
					furtherCitationDiffs,
					bibliographyDiffs,
					diffFormattedCitation,
					diffFormattedFurtherCitation,
					diffFormattedBibliography,
					cslData = data.get(),
					furtherCitationNode = data.getNodesFromPath("style/citation/layout", cslData),
					citationNode = data.getNodesFromPath("style/citation/layout", cslData),
					bibliographyNode = data.getNodesFromPath("style/bibliography/layout", cslData),
					furtherCitationOutput = false;

				statusOut.html("<i>Re-formatting citations...</i>");

				debug.time("formatCitations");

				exampleReferences = exampleReferences || CSLEDIT_exampleCitations.getCiteprocReferences();
				exampleCitations = exampleCitations || CSLEDIT_exampleCitations.getCitations();
				furtherCitationOutput =  typeof furtherCitationsOut !== 'undefined';

				formattedResult = formatCitations(style, exampleReferences, exampleCitations, true);

				debug.timeEnd("formatCitations");

				statusOut.html(formattedResult.statusMessage);

				// add syntax highlighting at highest level
				if (citationNode.length > 0) {
					// wrap in outer div since the .inline-csl-entry one is an inline-block
					citationTagStart =
						'<div class="csl-entry-container">' +
						'<div class="inline-csl-entry" cslid="' + citationNode[0].cslId + '">';
					citationTagEnd = '</div></div>';
				}
				if (furtherCitationNode.length > 0) {
					// wrap in outer div since the .inline-csl-entry one is an inline-block
					furtherCitationTagStart =
						'<div class="csl-entry-container">' +
						'<div class="inline-further-csl-entry" cslid="' + furtherCitationNode[0].cslId + '">';
					furtherCitationTagEnd = '</div></div>';
				}
				if (bibliographyNode.length > 0) {
					bibliographyTagStart =
						'<div class="csl-entry-container">' +
						'<div class="bibliography-csl-entry" cslid="' + bibliographyNode[0].cslId + '">';
					bibliographyTagEnd = '</div></div>';
				}

				oldFormattedCitation = newFormattedCitation;
				newFormattedCitation = citationTagStart;
				newFormattedCitation += formattedResult.formattedCitations.join(
					citationTagEnd + citationTagStart);
				newFormattedCitation += citationTagEnd;

				oldFormattedFurtherCitation = newFormattedFurtherCitation;
				newFormattedFurtherCitation = furtherCitationTagStart;
				newFormattedFurtherCitation += formattedResult.formattedFurtherCitations.join(
					furtherCitationTagEnd + furtherCitationTagStart);
				newFormattedFurtherCitation += furtherCitationTagEnd;

				oldFormattedBibliography = newFormattedBibliography;
				newFormattedBibliography = bibliographyTagStart;
				newFormattedBibliography += formattedResult.formattedBibliography.join(
					bibliographyTagEnd + bibliographyTagStart);
				newFormattedBibliography += bibliographyTagEnd;

				// lazy instantiation of diff_match_patch
				if (dmp === null) {
					dmp = new diff_match_patch();
				}

				citationDiffs =
					dmp.diff_main(stripTags(oldFormattedCitation, "span"), stripTags(newFormattedCitation, "span"));
				dmp.diff_cleanupSemantic(citationDiffs);
				diffFormattedCitation = unescape(CSLEDIT_diff.prettyHtml(citationDiffs));

				furtherCitationDiffs =
					dmp.diff_main(stripTags(oldFormattedFurtherCitation, "span"), stripTags(newFormattedFurtherCitation, "span"));
				dmp.diff_cleanupSemantic(furtherCitationDiffs);
				diffFormattedFurtherCitation = unescape(CSLEDIT_diff.prettyHtml(furtherCitationDiffs));

				bibliographyDiffs =
					dmp.diff_main(stripTags(oldFormattedBibliography, "span"), stripTags(newFormattedBibliography, "span"));
				dmp.diff_cleanupSemantic(bibliographyDiffs);
				diffFormattedBibliography = unescape(CSLEDIT_diff.prettyHtml(bibliographyDiffs));

				if (dmp.diff_levenshtein(citationDiffs) === 0 && dmp.diff_levenshtein(furtherCitationDiffs) === 0 && dmp.diff_levenshtein(bibliographyDiffs) === 0) {
					citationsOut.html(newFormattedCitation);
					if (furtherCitationOutput) {
						furtherCitationsOut.html(newFormattedFurtherCitation);
					}
					bibliographyOut.html(newFormattedBibliography);
					if (typeof callback !== "undefined") {
						callback();
					}
				} else {
					if (CSLEDIT_options.get('showDiffOnChange') === true) {
						// display the diff
						citationsOut.html(diffFormattedFurtherCitation);
						if (furtherCitationOutput) {
							furtherCitationsOut.html(diffFormattedCitation);
						}
						bibliographyOut.html(diffFormattedBibliography);

						// display the new version in 1000ms
						clearTimeout(diffTimeout);
						diffTimeout = setTimeout(
							function () {
								citationsOut.html(newFormattedCitation);
								if (furtherCitationOutput){
									furtherCitationsOut.html(newFormattedFurtherCitation);
								}
								bibliographyOut.html(newFormattedBibliography);
								if (typeof callback !== "undefined") {
									callback();
								}
							},
						1000);
					} else {
						// display the real result
						citationsOut.html(newFormattedCitation);
						if (furtherCitationOutput) {
							furtherCitationsOut.html(newFormattedFurtherCitation);
						}
						bibliographyOut.html(newFormattedBibliography);
						if (typeof callback !== "undefined") {
							callback();
						}
					}
				}

				if ("hangingIndent" in formattedResult) {
					bibliographyOut.find('.bibliography-csl-entry').css({
						"padding-left" : formattedResult.hangingIndent + "em",
						"text-indent" : "-" + formattedResult.hangingIndent + "em"
					});
				} else {
					bibliographyOut.find('.bibliography-csl-entry').css({
						"padding-left" : "0",
						"text-indent" : "0"
					});
				}

				debug.timeEnd("runCiteprocAndDisplayOutput");
			};

			/**
			 * Linearises the DOM representation of the Citeproc output.
			 *
			 * Prelinearising saving only the leafs and the ids / styles, the parents had
			 * 1. go to first leaf, save styles and ids in lists
			 * 2. Set lists in the found leaf
			 * 3. going up until no more parents or a node with siblings is reached, delete styles and ids from list
			 * 4. if node has sibling, visti this sibling repeat with 1.
			 * 5. if node has no child -> END
			 *
			 * @param citationDOM the Dom representation of the Citeproc Output
			 * @returns {Array} a list of attributes / delimiters / freeTexts with additional informations about parents
			 * @alias module:CSLEDIT_citationEngine.lineariseDOM
			 * @author Matthias Cosler
			 * @private
			 */
			var lineariseDOM = function (citationDOM) {
				//console.log(citationDOM);
				var leaflist = [],
					currentnode,
					currentID,
					cslIdList = [], //list of cslIDs and names of all the parents of the current node
					styleList = [], //list of style attributes of all the parents of the current node
					prefix, //prefix and suffix of the node
					suffix;
				//TODO prefix and suffix of groups??

					currentnode = citationDOM[0];


				/* prelinearising saving only the leafs and the ids / styles, the parents had
				* 1. go to first leaf, save styles and ids in lists
				* 2. Set lists in the found leaf
				* 3. going up until no more parents or a node with siblings is reached, delete styles and ids from list
				* 4. if node has sibling, visti this sibling repeat with 1.
				* 5. if node has no child -> END*/
				do {

					//got to first leaf, memorise styles and ids, even if they are undefined
					while ((currentnode.childNodes.length !== 0)) {
						cslIdList.push([$(currentnode).attr("cslid"), $(currentnode).attr("class")]);
						if (currentnode.tagName !== "SPAN" && currentnode.tagName !== "DIV") {
							styleList.push(currentnode.tagName);
						} else {
							styleList.push($(currentnode).attr("style"));
							//console.log($(currentnode).attr("style"));
						}
						currentnode = currentnode.firstChild;
					}
					//the last leaf is not really a dom object but the text itself and doesn't need to be added to the list
					//for conistency reasons add undefined
					cslIdList.push([undefined,undefined]);
					styleList.push(undefined);

					//filter all the undefined because we dont need the information in the leaf
					var filteredstyleList = styleList.filter(function (el) {
						return el !== null && el !== undefined;
					});
					var filteredCslIdList = cslIdList.filter(function (el) {
						return el[0] !== null && el[0] !== undefined && el[1] !== null && el[1] !== undefined;
					});


					//if there is at least one object with an cslID, we have a valid leaf
					if (filteredCslIdList.length>0) {

						var variableType = undefined; // the type of the variable
						var nodeType = undefined; //either delimiter or attribute or freeText but initially undefined
						currentID = parseInt(filteredCslIdList[filteredCslIdList.length - 1][0]);
						var currentCslNode = CSLEDIT_data.getNode(currentID);
						//detectType
						if ((hasAttributeKey(currentCslNode, "delimiter") !==-1) && (currentCslNode.name !== "names")){
							//is delimiter
							nodeType = "delimiter";
						} else if ((hasAttributeKey(currentCslNode, "value")!==-1) || ((hasAttributeKey(currentCslNode, "term")!==-1))) {
							//is freeText
							nodeType = "freeText";
						} else if (hasAttributeKey(currentCslNode, "variable")!==-1) {
							//is attribute
							nodeType = "attribute";
							//getVariableType
							var variableIndex = hasAttributeKey(currentCslNode, "variable");
							variableType = currentCslNode.attributes[variableIndex].value; //only set if attribute
						}

						var leaf = {
							styleList: filteredstyleList,
							cslIdList: filteredCslIdList,
							value: currentnode.nodeValue,
							type: nodeType, //could be undefinded (if unalbe to assign proper type)
							variableType: variableType, //could be undefined (if not attribute)
							currentID: currentID
						};
						leaflist.push(leaf);

					}
					while (currentnode.nextSibling === null && currentnode.parentNode !== null) {
						currentnode = currentnode.parentNode;
						cslIdList.pop();
						styleList.pop();
					}
					if (currentnode.nextSibling !== null) {
						currentnode = currentnode.nextSibling;
						cslIdList.pop();
						styleList.pop();
					}
				} while (currentnode.parentNode !== null);

				return leaflist;
			};

			/**
			 * Test if the attribute with the key keystring is in the cslNode
			 *
			 * @param cslNode
			 * @param keyString
			 * @returns {number} -1 if not else the index where it was found
			 * @alias module:CSLEDIT_citationEngine.hasAttributeKey
			 * @author Matthias Cosler
			 * @private
			 */
			var hasAttributeKey = function (cslNode, keyString) {
				var attributes = cslNode.attributes;
				var hasKeyIndex = -1;
				$.each(attributes, function (index, item) {
					if (item.hasOwnProperty("key")){
						if (item.key === keyString){
							hasKeyIndex = index;
						}
					}
				});
				return hasKeyIndex;
			};



			//TODO possible error in citeproc-js: When a variable is encased by a macro, the delimiter is parted from the variable,
			// when not the delimiter is connected to the variable i.e. is in the same span element. Unable to distinguisch between them by parsing them.
			// Except the variable has some format (like <i>) which requires a distinct element
			/**
			 * Gets the Dummy Citation Data, formats it and parses the output so it can be used to arrange dragndrop
			 *
			 *  - gets the dummyData References ({@link module:CSLEDIT_exampleCitations.getDummyCiteprocReferences}) and Citations ({@link module:CSLEDIT_exampleCitations.getDummyCitations})
			 *  - formats the citations using {@link module:CSLEDIT_citationEngine.formatCitations}
			 *  - linearises the output using {@link module:CSLEDIT_citationEngine.lineariseDOM}
			 *
			 * @param citationnumbers
			 * @param data the csl-data
			 * @returns {{formattedFurtherCitations: Array, formattedBibliography: Array, formattedCitations: Array, statusMessage: string}}  an object containing three lists of attributes / delimiters
			 * @example <caption>Example for the return value</caption>
			 * 	{
				  "statusMessage": "",
				  "formattedFurtherCitations": [{
				  	"styleList": ["I"],
				  	"cslIdList": [["622","macro"],["623","names"]],
				  	"value": "Autor",
				  	"type": "attribute",
				  	"variableType": "author",
				  	"currentID": 623}],
				  "formattedCitations": [{
					"styleList": ["I"],
					"cslIdList": [["672","macro"],["673","names"]],
					"value": "Autor",
					"type": "attribute",
					"variableType": "author",
					"currentID": 673}, ...],
				  "formattedBibliography": [{
				  	"styleList": ["I"],
					"cslIdList": [["577","macro"],["578","names"]],
					"value": "Autor",
					"type": "attribute",
					"variableType": "author",
					"currentID": 578}, ...]
				}
			 * @alias module:CSLEDIT_citationEngine.runCiteprocAndParseOutput
			 * @author Matthias Cosler
			 * @public
			 */
			var runCiteprocAndParseOutput = function (citationnumbers, data) {

				debug.time("runCiteprocAndParseOutput");

				var style = data.getCslCode(),
					exampleCitations,
					exampleReferences,
					formattedResult,
					result = { "statusMessage": "", "formattedFurtherCitations": [], "formattedCitations": [], "formattedBibliography": [] };


				console.log("<i>Re-formatting citations...</i>");

				debug.time("formatCitations");

				exampleReferences = CSLEDIT_exampleCitations.getDummyCiteprocReferences();
				exampleCitations = CSLEDIT_exampleCitations.getDummyCitations(citationnumbers);

				formattedResult = formatCitations(style, exampleReferences, exampleCitations, true);

				debug.timeEnd("formatCitations");
				debug.time("parseCitationOutput");

				result = { "statusMessage": "", "formattedFurtherCitations": [], "formattedCitations": [], "formattedBibliography": [] };

				result.statusMessage = formattedResult.statusMessage;

				if (formattedResult.formattedBibliography.length > 0) {
					result.formattedBibliography = lineariseDOM($.parseHTML(formattedResult.formattedBibliography[0]));
				} else {
					result.formattedBibliography = [];
				}

				if (formattedResult.formattedCitations.length > 0) {
					result.formattedCitations = lineariseDOM($.parseHTML(formattedResult.formattedCitations[0]));
				} else {
					result.formattedCitations = [];
				}

				if (formattedResult.formattedFurtherCitations.length > 0) {
					result.formattedFurtherCitations = lineariseDOM($.parseHTML(formattedResult.formattedFurtherCitations[0]));
				} else {
					result.formattedFurtherCitations = [];
				}


				debug.timeEnd("parseCitationOutput");

				console.log(formattedResult.statusMessage);

				debug.timeEnd("runCiteprocAndParseOutput");

				return result;
			};



	// Public members:
	return {
		formatCitations : formatCitations,
		runCiteprocAndDisplayOutput : runCiteprocAndDisplayOutput,
		runCiteprocAndParseOutput : runCiteprocAndParseOutput
	};

});
