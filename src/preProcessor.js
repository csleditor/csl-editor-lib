"use strict";
/**
 * Provides Functions which should be called before opening the law editor. Guarantees that the law editor has full functuality.
 *
 * @see module:CSLEDIT_data
 * @see module:CSLEDIT_CslNode
 * @see module:CSLEDIT_dummyData
 * @module CSLEDIT_preProcessor
 * @todo make faster (see issue csleditor/csl-editor-lib#1)
 * @author Matthias Cosler
 */
define(['src/dataInstance',
        'src/CslNode',
        'src/dummyDataHandler',
        'jquery'
    ],
    function (
        CSLEDIT_data,
        CSLEDIT_CslNode,
        CSLEDIT_dummyData,
        $
    ) {

        /**
         * Since childs are lists of objects and every object has a name, this function returns all (direct) child with the chosen name.
         * return empty list if not present
         *
         *  - iterate through children and return all children with the given name
         *
         * @param node the node which childs are looked for
         * @param name the name of the child which is wanted
         * @returns {array} a list with all the childs of node having the wanted name. Empty list if name not present
         * @alias module:CSLEDIT_preProcessor.getChildsWithName
         * @author Matthias Cosler
         * @private
         */
        var getChildsWithName = function (node, name) {
            var ret = [];
            $.each(node.children, function (index, item) {
                if (item.name === name){
                    ret.push(item);
                }
            });
            return ret;
        };

        /**
         * Inserts the ifFirst term in the needed form.
         *
         * If First will be added directly under citations -> layout into the csl-tree.
         * The former children of this layout node will be moved / duplicated as children of the newly inserted "if first and the "else" node
         *
         *  - generate new {@link module:CSLEDIT_CslNode}s for the if first, the else and the choose
         *  - append the nodes directly under the layout Node using {@link module:CSLEDIT_data.addNode}
         *  - for every children of the old layout node, copy the children (and their children recursively) under the if first Node using {@link module:CSLEDIT_preProcessor.copyNode}
         *  - for every children of the old layout node, move the children (and their children recursively) under the else Node using {@link module:CSLEDIT_data.moveNode}
         *  - duplicate all macros which are used in the if first part using {@link module:CSLEDIT_preProcessor.duplicateMacros}
         *
         * @param {number} layoutID the Id of the layout Node under which ifFirst will be added
         * @alias module:CSLEDIT_preProcessor.insertIfFirst
         * @author Matthias Cosler
         * @private
         */
        var insertIfFirst = function (layoutID) {
            var childrenNodeIf = new CSLEDIT_CslNode({
                name : "if",
                attributes : [
                    {
                        "key": "match",
                        "value": "any",
                        "enabled": true
                    },
                    {
                        "key": "position",
                        "value": "first",
                        "enabled": true
                    }
                ],
                children : []
            });
            var childrenNodeElse = new CSLEDIT_CslNode({
                name : "else",
                attributes : [],
                children : []
            });
            var newNode = new CSLEDIT_CslNode({
                name : "choose",
                attributes : [],
                children : []
            });

            var childrenID = CSLEDIT_data.addNode(layoutID, "first", newNode).args[0];
            var ifID = CSLEDIT_data.addNode(childrenID, "first", childrenNodeIf).args[0];
            var oldNodes = CSLEDIT_data.getNode(layoutID).children.slice(1);
            $.each(oldNodes, function (index, item) {
                copyNode(item, ifID, "last");
            });
            modifyMacroNames(CSLEDIT_data.getNode(ifID), "first-");
            var elseID = CSLEDIT_data.addNode(childrenID, "last", childrenNodeElse).args[0];
            oldNodes = CSLEDIT_data.getNode(layoutID).children.slice(1);
             $.each(oldNodes, function (index, item) {
                 CSLEDIT_data.moveNode(item.cslId, elseID, "last");
            });
            duplicateMacros(CSLEDIT_data.getNode(ifID), "first-");
        };


        /**
         * Duplicates the node with the given fromCslId and inserts the copy into the position composed by "toCslId" and "position".
         * Generates a deep copy, i.e. copies all child nodes as well recursively
         *
         * - creating a new Node with the characteristics of the old Node
         * - adding New Node at the correct postion using {@link module:CSLEDIT_data.addNode}
         * - copying all chldrens recursivley using this function
         *
         * @param fromNode the Node which should be copied
         * @param toCslId the ID to which it should be copied
         * @param position , relative to this position
         * @example <caption> Position can be one of the following values:</caption>
         * - integer  - the child index within the existing node
         * - "first"  - the first child of the existing node
         * - "last"   - the last child of the existing node
         * - "inside" - same as "last"
         * - "before" - the sibling before the existing node
         * - "after"  - the sibling after the existing node
         *
         * @alias module:CSLEDIT_preProcessor.copyNode
         * @author Matthias Cosler
         * @private
         */
        var copyNode = function (fromNode, toCslId, position) {
            var oldNode = new CSLEDIT_CslNode(fromNode);
            var newAttributes = JSON.parse(JSON.stringify(oldNode.attributes));

            var newNode = new CSLEDIT_CslNode({
                name: oldNode.name,
                attributes : newAttributes,
                textValue : oldNode.textValue,
                children : []
            });

            var newToCslId = CSLEDIT_data.addNode(toCslId, position, newNode).args[0];
            $.each(oldNode.children, function (index, item) {
                copyNode(item, newToCslId, "last");
            });
        };

        /**
         * Copies a macro, including its children. Since macro names are unique, the new macro will have a unique newMacroName.
         *
         *  - gets the old Macro using {@link module:CSLEDIT_preProcessor.getMacro}
         *  - tests if the new Macro name isn't already in use
         *  - create new Macro with the new Name but all other characteristics from the old Macro
         *  - add the new Macro on the macro level using {@link module:CSLEDIT_data.addNode}
         *  - copies all children of hte ols macro under the new Macro using {@link module:CSLEDIT_preProcessor.copyNode}
         *
         * @param macroName the current name of the macro
         * @param newMacroName the name of the generated macro
         * @alias module:CSLEDIT_preProcessor.copyMacro
         * @author Matthias Cosler
         * @private
         */
        var copyMacro = function (macroName, newMacroName){
            var macro = new CSLEDIT_CslNode(getMacro(macroName));
            if (getMacro(newMacroName) !== undefined){
                throw "the name for the macro which should be duplicated is already present";
            }
            var newMacro = new CSLEDIT_CslNode({
                name : "macro",
                attributes : [
                    {
                        key: "name",
                        value: newMacroName,
                        enabled: true
                    }
                ],
                children : []
            });
            var newMacroId = CSLEDIT_data.addNode(macro.cslId, "after", newMacro).args[0];
            $.each(macro.children, function (index, item) {
                copyNode(item, newMacroId, "last");
            });
        };

        /**
         * Gets a macro definition node with the given macroName.
         *
         * - search all macros for the given name as Attribute
         *
         * @param macroName the name of the macro which is wanted
         * @returns {*} the definition node or undefined if not present
         * @alias module:CSLEDIT_preProcessor.getMacro
         * @author Matthias Cosler
         * @private
         */
        var getMacro = function (macroName) {
            var csl_data = CSLEDIT_data.get();
            var macros = getChildsWithName(csl_data, "macro");
            var ret;
            $.each(macros, function (index, item) {
                item = new CSLEDIT_CslNode(item);
                if (item.getAttr("name")=== macroName){
                    ret = item;
                    return false;
                }
            });
            return ret;
        };

        /**
         * Search for Macros in the section beginning at node and add a prefix to their names
         *
         *  - iterate through csl-tree beginning at node.
         *  - for each node test if it is a macro reference. If yes:
         *      - generate the Name for the new Macro by adding the Prefix
         *      - change the reference to the new MacroName using {@link module:CSLEDIT_data.amendNode}
         *  - recursively continue with the children
         *
         * @param node the node from the citation section where the search for macros to rename is started
         * @param prefix the prefix, which is added to the macroName, in Order to rename it uniquely
         * @alias module:CSLEDIT_preProcessor.modifyMacroNames
         * @author Matthias Cosler
         * @private
         */
        var modifyMacroNames = function (node, prefix) {
            node = new CSLEDIT_CslNode(node);
            if (node.hasAttr("macro")) {
                var oldMacroName = node.getAttr("macro");
                var newMacroName = prefix + oldMacroName;
                $.each(node.attributes, function (index, item) {
                    if (item.key === "macro") {
                        item.value = newMacroName;
                    }
                    return false;
                });
                CSLEDIT_data.amendNode(node.cslId, node);
            }
            $.each(node.children, function (index, item) {
                modifyMacroNames(item, prefix);
            });
        };

        /**
         * Duplicates all Macros which are used under the node node
         *
         * - iterate through the csl-tree and search for macros references
         * - if a macro reference is found
         *      - get the macro using {@link module:CSLEDIT_preProcessor.getMacro}
         *      - copy the macro using {@link module:CSLEDIT_preProcessor.copyMacro}
         * - recursively continue with children
         *
         * @param node the node where the search for macros to duplicate is started
         * @param prefix the prefix which was added to the macro
         * @alias module:CSLEDIT_preProcessor.duplicateMacros
         * @author Matthias Cosler
         * @todo to fasten the complete preProcessor it would be better to call this after it is rectified
         * @private
         */
        var duplicateMacros = function (node, prefix) {
            node = new CSLEDIT_CslNode(node);
            if (node.hasAttr("macro")) {
                var newMacroName = node.getAttr("macro");
                var sublength = prefix.length;
                var oldMacroName = newMacroName.substring(sublength);
                if (getMacro(newMacroName) === undefined) {
                    copyMacro(oldMacroName, newMacroName);
                }
            }

            $.each(node.children, function (index, item) {
                duplicateMacros(item, prefix);
            });
        };

        /**
         * Tests if the csl-code below cslID is in the needed form regarding ifFirst.
         *
         * Tests if the code has exactly the form that insertIfIFirst would have generated
         *
         * @param cslId the id at which the form is wanted
         * @returns {boolean} true if it has the needed form
         * @alias module:CSLEDIT_preProcessor.testIfFirstForm
         * @author Matthias Cosler
         * @private
         */
        var testIfFirstForm = function (cslId) {
            var testNode = CSLEDIT_data.getNode(cslId);
            if (!((testNode.children.length > 0) &&
                (testNode.children[0].name === "choose") &&
                (testNode.children[0].children.length === 2 ) &&
                (testNode.children[0].children[0].name === "if") &&
                (testNode.children[0].children[1].name === "else") &&
                (testNode.children[0].children[0].attributes.length === 2))){
                return false;
            }
            return (
                (
                    (testNode.children[0].children[0].attributes[0].key === "match") &&
                    (testNode.children[0].children[0].attributes[0].value === "any") &&
                    (testNode.children[0].children[0].attributes[0].enabled === true) &&
                    (testNode.children[0].children[0].attributes[1].key === "position") &&
                    (testNode.children[0].children[0].attributes[1].value === "first") &&
                    (testNode.children[0].children[0].attributes[1].enabled === true)
                ) || (
                    (testNode.children[0].children[0].attributes[1].key === "match") &&
                    (testNode.children[0].children[0].attributes[1].value === "any") &&
                    (testNode.children[0].children[0].attributes[1].enabled === true) &&
                    (testNode.children[0].children[0].attributes[0].key === "position") &&
                    (testNode.children[0].children[0].attributes[0].value === "first") &&
                    (testNode.children[0].children[0].attributes[0].enabled === true)

                )
            );
        };

        /**
         * Removes all if firsts inside the highest if first tag
         *
         * @param layoutID
         * @alias module:CSLEDIT_preProcessor.rectifyIfFirst
         * @todo not implemented (see issue csleditor/csl-editor-lib#20)
         * @private
         */
        var rectifyIfFirst = function (layoutID) {
            //TODO one should remove all occurences of the tag if first inside the first occurrences.
            // For the if case all if first is standard and the rest should be removed.
            // For the else case all if first should be removed.
            // special cases: if first inside match any or match all...
            // not implemented
        };

        /**
         * This function will handle the "if position first".
         *
         * - first it will be tested if the csl data is already in the correct form (see {@link module:CSLEDIT_preProcessor.testIfFirstForm})
         * - if not it will generate a choose tag on the highest possible level. This choose tag will part the decision tree in handling the "if first" occurence and handling the rest (see {@link module:CSLEDIT_preProcessor.insertIfFirst }).
         * - In a second step, we will remove all unnecessary parts from each conditional (see {@link module:CSLEDIT_preProcessor.rectifyIfFirst})
         *
         * This is necessary, to automatically change nodes by using the law editor in the first occurrence field
         *
         * @alias module:CSLEDIT_preProcessor.handleIfFirst
         * @author Matthias Cosler
         * @public
         */
        var handleIfFirst  = function () {
            var csl_data = CSLEDIT_data.get();
            var layoutID = getChildsWithName(getChildsWithName(csl_data, "citation")[0],"layout")[0].cslId;
            if (!testIfFirstForm(layoutID)){
                insertIfFirst(layoutID);
                rectifyIfFirst(layoutID);
            }
        };

        /**
         * To assure that editing in one source section does not modify other sections accidently, this procedure will generate a conditional for all these sources on the top level.
         *
         * For every position (iffirst, iffirst else, bibliography) the following is called:
         *  - get the datat about all the sections from {@link module:CSLEDIT_dummyData.cslNames}
         *  - first it will be tested if the csl data is already in the correct form (see {@link module:CSLEDIT_preProcessor.testSourceSplittedForm})
         *  - If not, the neccesarry data will be duplicated for every source and attached to the conditional (see {@link module:CSLEDIT_preProcessor.insertSourceSplitting }).
         *  - In a second step, we will remove all unnecessary parts from each conditional (see {@link module:CSLEDIT_preProcessor.rectifySourceDuplication})
         *
         * This is necessary, to automatically change nodes by using the law editor in the differnet sections
         *
         * @alias module:CSLEDIT_preProcessor.handleSourceSplitting
         * @author Matthias Cosler
         * @public
         */
        var handleSourceSplitting = function () {
            //get neccesaary dummmyData for SourceSections
            var sourceSections = [];
            var dummyCsl = CSLEDIT_dummyData.cslNames();
            $.each(dummyCsl, function (index, item) {
                sourceSections.push(CSLEDIT_dummyData.replaceWhitechar(item.type));
            });

            //handle SourceSplitting in Layout
            //if first case
            var csl_data = CSLEDIT_data.get();
            var cslID = getChildsWithName(getChildsWithName(getChildsWithName(getChildsWithName(csl_data, "citation")[0],"layout")[0], "choose")[0], "if")[0].cslId;
            if (!testSourceSplittedForm(cslID, sourceSections)){
                insertSourceSplitting("first", sourceSections);
                rectifySourceDuplication(cslID);
            }
            //if first-else case
            csl_data = CSLEDIT_data.get();
            cslID = getChildsWithName(getChildsWithName(getChildsWithName(getChildsWithName(csl_data, "citation")[0],"layout")[0], "choose")[0], "else")[0].cslId;
            if (!testSourceSplittedForm(cslID, sourceSections)){
                insertSourceSplitting("else", sourceSections);
                rectifySourceDuplication(cslID);
            }
            //handle SourceSplitting in Bibliography
            csl_data = CSLEDIT_data.get();
            cslID = getChildsWithName(getChildsWithName(csl_data, "bibliography")[0],"layout")[0].cslId;
            if (!testSourceSplittedForm(cslID, sourceSections)){
                insertSourceSplitting("bib", sourceSections);
                rectifySourceDuplication(cslID);
            }
        };

        /**
         * Tests if the csl-code has the needed form regarding the Source Section Splitting
         *
         * Tests if the code below cslID has exactly the form that insertSourceSplitting would have generated
         *
         * @param cslId the clsid under which the splitting is wanted
         * @param sections the sections on which basis the splitting will be done
         * @returns {boolean} true iff the childs of the node with cslID has the necessary form
         *
         * @alias module:CSLEDIT_preProcessor.testSourceSplittedForm
         * @author Matthias Cosler
         * @private
         */

        var testSourceSplittedForm  = function (cslId, sections) {
            var testNode = CSLEDIT_data.getNode(cslId);
            if (sections.length === 0){
                return true; //lazy evaluation
            } else if (sections.length === 1){
                if (!((testNode.children.length > 0) &&
                    (testNode.children[0].name === "choose") &&
                    (testNode.children[0].children.length === 2 ) &&
                    (testNode.children[0].children[0].name === "if") &&
                    (testNode.children[0].children[1].name === "else") &&
                    (testNode.children[0].children[0].attributes.length === 2))){
                    return false; //lazy evaluation
                }
            } else if (sections.length >= 1){
                if ((testNode.children.length > 0) &&
                    (testNode.children[0].name === "choose") &&
                    (testNode.children[0].children.length === (sections.length +1) ) &&
                    (testNode.children[0].children[0].name === "if")){
                    var correct = true;
                    for (var i = 1; i < sections.length; i++) {
                        correct = correct && (testNode.children[0].children[i].name === "else-if");
                        if (!correct){
                            break; //lazy evaluation
                        }
                    }
                    correct = correct && (testNode.children[0].children[sections.length].name === "else");
                    if (!correct){
                        return false; //lazy evaluation
                    }
                }
            }

            var attributesCorrect = true;
            $.each(sections, function (index, section) {
                attributesCorrect = attributesCorrect && testNode.children[0].children[index].attributes.length === 2;
                if (!attributesCorrect){
                    return false; //lazy evaluation
                }
                attributesCorrect = attributesCorrect &&
                    (
                        (testNode.children[0].children[index].attributes[0].key === "match") &&
                        (testNode.children[0].children[index].attributes[0].value === "any") &&
                        (testNode.children[0].children[index].attributes[0].enabled === true) &&
                        (testNode.children[0].children[index].attributes[1].key === "type") &&
                        (testNode.children[0].children[index].attributes[1].value === section) &&
                        (testNode.children[0].children[index].attributes[1].enabled === true)
                    ) || (
                        (testNode.children[0].children[index].attributes[1].key === "match") &&
                        (testNode.children[0].children[index].attributes[1].value === "any") &&
                        (testNode.children[0].children[index].attributes[1].enabled === true) &&
                        (testNode.children[0].children[index].attributes[0].key === "type") &&
                        (testNode.children[0].children[index].attributes[0].value === section) &&
                        (testNode.children[0].children[index].attributes[0].enabled === true)
                    );
                if (!attributesCorrect){
                    return false; //lazy evaluation
                }

            });
            return attributesCorrect;
        };

        /**
         * Inserts the SourceSplitting.
         *
         * For each Source Section a case of a conditional is generated. Then the code gets duplicated for every source and attached to his own conditional case.
         *
         *  - generate {@link module:CSLEDIT_CslNode}s for the if for the first section, and the choose
         *  - add these nodes to the csl-data using {@link module:CSLEDIT_data.addNode} at the given position
         *  - for every children of the old node, copy the children (and their children recursively) under the if first Node using {@link module:CSLEDIT_preProcessor.copyNode}
         *  - for each other section do:
         *      - generate {@link module:CSLEDIT_CslNode}s for the else if case
         *      - add these nodes to the csl-data using {@link module:CSLEDIT_data.addNode} at the given position
         *      - for every children of the old node, copy the children (and their children recursively) under the if first Node using {@link module:CSLEDIT_preProcessor.copyNode}
         *  - generate {@link module:CSLEDIT_CslNode}s for the else (default) case
         *  - for every children of the old node, move the children (and their children recursively) under the else Node using {@link module:CSLEDIT_data.moveNode}
         *  - duplicate all macros which are used in the if first part using {@link module:CSLEDIT_preProcessor.duplicateMacros}
         *
         * @param position the point at which the SourceSplitting will be inserted. See {@link module:CSLEDIT_preProcessor.getNodeFromPosition} for further information
         * @param sections the sections on which basis the splitting will be done
         * @alias module:CSLEDIT_preProcessor.insertSourceSplitting
         * @author Matthias Cosler
         * @private
         */
        var insertSourceSplitting = function (position, sections) {
            var cslID = getNodeFromPosition(position).cslId;
            var bibprefix;
            if (position === "bib"){
                bibprefix = "bib-";
            } else {
                bibprefix = "";
            }
            var childrenNodeIf = new CSLEDIT_CslNode({
                name : "if",
                attributes : [
                    {
                        "key": "match",
                        "value": "any",
                        "enabled": true
                    },
                    {
                        "key": "type",
                        "value": sections[0],
                        "enabled": true
                    }
                ],
                children : []
            });

            var newNode = new CSLEDIT_CslNode({
                name : "choose",
                attributes : [],
                children : []
            });
            var childrenID = CSLEDIT_data.addNode(cslID, "first", newNode).args[0];
            var ifID = CSLEDIT_data.addNode(childrenID, "last", childrenNodeIf).args[0];
            var oldNodes = CSLEDIT_data.getNode(cslID).children.slice(1);
            $.each(oldNodes, function (index, item) {
                copyNode(item, ifID, "last");
            });
            modifyMacroNames(CSLEDIT_data.getNode(ifID), sections[0] + "-" + bibprefix);

            $.each(sections, function (sectionIndex, section) {
                if (sectionIndex===0){
                    return true; //start with second, because first is if case
                }
                var childrenNodeElseIf = new CSLEDIT_CslNode({
                    name : "else-if",
                    attributes : [
                        {
                            "key": "match",
                            "value": "any",
                            "enabled": true
                        },
                        {
                            "key": "type",
                            "value": section,
                            "enabled": true
                        }
                    ],
                    children : []
                });

                var elseIfID = CSLEDIT_data.addNode(childrenID, "last", childrenNodeElseIf).args[0];
                $.each(oldNodes, function (index, item) {
                    copyNode(item, elseIfID, "last");
                });
                modifyMacroNames(CSLEDIT_data.getNode(elseIfID), section + "-" + bibprefix);

            });

            var childrenNodeElse = new CSLEDIT_CslNode({
                name : "else",
                attributes : [],
                children : []
            });

            var elseID = CSLEDIT_data.addNode(childrenID, "last", childrenNodeElse).args[0];
            oldNodes = CSLEDIT_data.getNode(cslID).children.slice(1);
            $.each(oldNodes, function (index, item) {
                CSLEDIT_data.moveNode(item.cslId, elseID, "last");
            });

            $.each(sections, function (sectionIndex, section) {

                var parentNode = getNodeFromPosition(position);
                var node = getChildsWithName(parentNode, "choose")[0].children[sectionIndex];
                duplicateMacros(node, section + "-" + bibprefix);
            });


        };

        /**
         * Gets the node with the wanted position
         *
         * Gets the parent Node from cslData which contains as children all the parts inside
         *  - the if first (first)
         *  - the else form if first (else)
         *  - the bibliography layout (bib)
         *
         * @param position is either "first", "else" or "bib"
         * @returns a cslNode
         *
         * @alias module:CSLEDIT_preProcessor.getNodeFromPosition
         * @author Matthias Cosler
         * @private
         */
        var getNodeFromPosition = function(position){
            var csl_data = CSLEDIT_data.get();
            if (position === "first"){
                return getChildsWithName(getChildsWithName(getChildsWithName(getChildsWithName(csl_data, "citation")[0],"layout")[0], "choose")[0], "if")[0];
            } else if (position === "else"){
                return getChildsWithName(getChildsWithName(getChildsWithName(getChildsWithName(csl_data, "citation")[0],"layout")[0], "choose")[0], "else")[0];
            } else if (position === "bib") {
                return getChildsWithName(getChildsWithName(csl_data, "bibliography")[0],"layout")[0];
            }
        };

        /**
         * Removes all source conditionals form inside the wrong condition
         *
         * @param cslID the point at which the SourceSplitting conditionals are.
         *
         * @alias module:CSLEDIT_preProcessor.rectifySourceDuplication
         * @todo not implemented (see issue csleditor/csl-editor-lib#20)
         * @private
         */
        var rectifySourceDuplication = function (cslID) {
            //TODO one should remove all occurences of the SourceCondition inside the wrong SourceCondition
            // Iterate through the Duplicates and remove all occurences of Source Conditions inside it.
            // Set the Descendants of the Condition which is already chosen due to the conditon on the highest level to standard
            // and remove the descendents of the other conditions.
            // For the if case all if first is standard and the rest
            // special cases: if condition case inside match any or match all...
            // not implemented
        };

        /**
         * Removes all macros which are not used inside citation or bibliography.
         *
         * Such macros could be generated by the duplicating and rectifying process.
         * Could also be used in a general case to simplify/beautify the csl code
         *
         * @alias module:CSLEDIT_preProcessor.removeUnusedMacros
         * @todo not implemented (see issue csleditor/csl-editor-lib#20)
         * @public
         */
        var removeUnusedMacros = function () {
            //TODO (not necessary if duplicateMacros is called after teh rectifying preocess
        };


        /**
         * This function will remove all suffixes and prefixes, since they won't be supported by the law editor.
         * Suffixes and Prefixes will be transformed to free texts or delimiters
         *
         * @alias module:CSLEDIT_preProcessor.removeAffix
         * @todo not implemented (see issue csleditor/csl-editor-lib#8)
         * @public
         */
        var removeAffix  = function () {
            //TODO not implemented
        };

        /**
         * This function will encapsulate all directly used variables into macros. The Macros will be called after their encapsuled variable and identifiable suffix.
         *
         * @alias module:CSLEDIT_preProcessor.addMacros
         * @todo not implemented (see issue csleditor/csl-editor-lib#9)
         * @public
         */
        var addMacros  = function () {
            //TODO not implemented
        };

        /**
         * This functions runs the preprocessor. All necessary functions will be called in the right order:
         *
         *  - {@link module:CSLEDIT_preProcessor.addMacros}
         *  - {@link module:CSLEDIT_preProcessor.removeAffix}
         *  - {@link module:CSLEDIT_preProcessor.handleIfFirst}
         *  - {@link module:CSLEDIT_preProcessor.handleSourceSplitting}
         *  - {@link module:CSLEDIT_preProcessor.removeUnusedMacros}
         *
         * @alias module:CSLEDIT_preProcessor.run
         * @author Matthias Cosler
         * @public
         */
        var run = function () {
            addMacros();
            removeAffix();
            handleIfFirst();
            handleSourceSplitting();
            removeUnusedMacros();
        };




        return {
            /**
             * @alias module:CSLEDIT_preProcessor.ifFirstSplitting
             * @see module:CSLEDIT_preProcessor.handleIfFirst
             */
            ifFirstSplitting : handleIfFirst,
            /**
             * @alias module:CSLEDIT_preProcessor.sourceSplitting
             * @see module:CSLEDIT_preProcessor.handleSourceSplitting
             */
            sourceSplitting : handleSourceSplitting,
            removeAffix : removeAffix,
            removeUnusedMacros: removeUnusedMacros,
            addMacros : addMacros,
            run : run
        };
    });