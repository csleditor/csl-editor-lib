"use strict";

/**
 * Wraps the {@link module:CSLEDIT_dummyRawData} and adds some functionality
 * @module CSLEDIT_dummyData
 * @see module:CSLEDIT_dummyRawData
 */
define(['src/dummyData',
        'jquery'
    ],
    function (
        CSLEDIT_dummyRawData,
        $
    ) {


        /**
         * Test if object has more properties. In this case there isn't a property anymore if the object is a string.
         *
         * Unfortunately Javascript handles the chars of a string as "properties"
         * or the "for in" notation works also for strings
         * -> have to decide whether it is a string or not.
         * @param obj the object which is to be tested
         * @return {boolean}
         * @private
         * @alias module:CSLEDIT_dummyData.hasProperties
         * @author Matthias Cosler
         * @private
         */
        var hasProperties = function(obj){
            return typeof obj !== 'string';
        };


        /**
         * Extract the one of the child elements by keeping the object structure but removing leaf-siblings
         *
         * @param obj the object on which the extraction should work
         * @param number Determines if the first second or thirs leaf is kept
         * @returns  the modified object and a flag needed for the recursion
         * @alias module:CSLEDIT_dummyData.extractElement
         * @author Matthias Cosler
         * @private
         */
        var extractElement = function(obj, number){
            if (hasProperties(obj)){
                for (var prop in obj) {
                    if (!obj.hasOwnProperty(prop)) {
                        continue; //The current property is not a direct property of item
                    }
                    var res = extractElement(obj[prop], number);
                    if (res[1]) {
                        obj = obj[number];
                        /*if (number===0){
                            //removes the dash from titles because of a possible bug in the processor
                            obj = obj.replace(/-/g, ' ');
                        }*/
                        return [obj, false];
                    } else {
                        obj[prop] = res[0];
                    }
                }
                return [obj, false];
            } else {
                return [obj,true];
            }
        };

        /**
         * Gets an Array which contains all the references.
         * Since the DummyData contains not only the references (like the exampleData does) the references need to be extracted first.
         * The content of the refernces should be the CSL-Names
         *
         * @returns {Array} an Array with all the Dummy references
         * @alias module:CSLEDIT_dummyData.references
         * @author Matthias Cosler
         * @public
         */
         var references = function () {
            var lawNamesR = lawNames();
            var cslNamesR = cslNames();
            $.each(lawNamesR, function (index, item) {
                item.type = cslNamesR[index].type;
            });
            return lawNamesR;
        };

        /**
         * Not needed anymore
         * @param attributeName
         * @returns {*}
         * @alias module:CSLEDIT_dummyData.replaceWhitechar
         * @author Matthias Cosler
         * @public
         */
        var replaceWhitechar = function (attributeName) {
            return attributeName/*.replace(/\s/g,'-')*/;
        };

        /**
         * Returns all the CSL-Names in the reference format
         * @returns {Array} an Array with all the CSL-Names
         * @alias module:CSLEDIT_dummyData.cslNames
         * @author Matthias Cosler
         * @public
         */
        var cslNames = function () {
            var result = [];
            $.each(jsonDocumentList, function (index, item) {
                var obj = JSON.parse(JSON.stringify(item)); //clone
                extractElement(obj,0);
                result.push(obj);
            });
            return result;
        };


        /**
         * Returns all the Zotero-Names in the reference format.
         * The Zotero Names, are the fileds which are used in the Zotero Client
         *
         * @returns {Array} an Array with all the Zotero-Names
         * @alias module:CSLEDIT_dummyData.zoteroNames
         * @author Matthias Cosler
         * @public
         */
        var zoteroNames = function () {
            var result = [];
            $.each(jsonDocumentList, function (index, item) {
                var obj = JSON.parse(JSON.stringify(item));
                extractElement(obj,1);
                result.push(obj);
            });
            return result;
        };

        /**
         * Returns all the Law-Names in the reference format.
         * Law Names are the identifiers of the csl-fields which are human readable and adjusted for their meaning in the german law
         *
         * @returns {Array} an Array with all the Law-Names
         * @alias module:CSLEDIT_dummyData.lawNames
         * @author Matthias Cosler
         * @public
         */
        var lawNames = function () {
            var result = [];
            $.each(jsonDocumentList, function (index, item) {
                var obj = JSON.parse(JSON.stringify(item));
                extractElement(obj,2);
                result.push(obj);
            });
            return result;
        };

        /**
         * Returns all the Descriptions in the reference format.
         * The Description is used to describe the Meaning and Purpose of this field, adjusted for the german law
         *
         * @returns {Array} an Array with all the Law-Names
         * @alias module:CSLEDIT_dummyData.descriptions
         * @author Matthias Cosler
         * @public
         */
        var descriptions = function () {
            var result = [];
            $.each(jsonDocumentList, function (index, item) {
                var obj = JSON.parse(JSON.stringify(item));
                extractElement(obj,3);
                result.push(obj);
            });
            return result;
        };

        /**
         * A Hard Coded List of all the relevant Typed of Citations (Book, Statute...) for the German Law
         * All Types listed here will be on the LawEditor Page in the Dragn'Drop Section
         * The Types contain all relevant fields for the german law. The CSL-Name, Zotero-Name, Law-Name, Description are saved here.
         * For better understanding of what the fields are see {@link module:CSLEDIT_dummyData.lawNames} , ...
         * @alias module:CSLEDIT_dummyData.jsonDocumentList
         * @author Matthias Cosler
         * @public
         */
        var jsonDocumentList = JSON.parse(JSON.stringify(CSLEDIT_dummyRawData.jsonDocumentList));


        return {
            jsonDocumentList : jsonDocumentList,
            descriptions : descriptions,
            lawNames : lawNames,
            zoteroNames : zoteroNames,
            cslNames : cslNames,
            replaceWhitechar: replaceWhitechar,
            references: references
        };
});
