"use strict";


/**
 * Provides the LawEditor with the Data needed to initialize the CitationTypeSections
 * @module CSLEDIT_initData
 * @see module:CSLEDIT_dummyData
 * @see module:CSLEDIT_citationEngine
 * @see module:CSLEDIT_data
 */
define(['src/dummyDataHandler',
        'src/citationEngine',
        'src/dataInstance',
        'jquery'
    ],
    function (
        CSLEDIT_dummyData,
        CSLEDIT_citationEngine,
        CSLEDIT_data,
        $
    ) {

        /**
         * A list of standard delimiters which will be added to the delimiterStack
         * @type {string[]}
         * @author Matthias Cosler
         * @private
         * @alias module:CSLEDIT_initData.standardDelimiters
         */
        var standardDelimiters = [", ", "/ "];

        /**
         * A list of standard free texts which will be added to the freetextStack
         * @type {string[]}
         * @author Matthias Cosler
         * @private
         * @alias module:CSLEDIT_initData.standardFreeTexts
         */
        var standardFreeTexts = ["."];



        /**
         * Extracts the first Leaf from a tree
         *
         *
         * @param obj the Object tree
         * @returns {string} the first Leaf
         * @private
         * @author Matthias Cosler
         * @alias module:CSLEDIT_initData.extractLeaf
         */
        var extractLeaf = function (obj) {
            if (typeof obj !== "string") {
                for (var prop in obj) {
                    if (!obj.hasOwnProperty(prop)) {
                        continue; //The current property is not a direct property of item
                    }
                    return extractLeaf(obj[prop]);
                }
            } else {
                return obj;
            }
        };

        /**
         * Deletes all Nodes which are not on the first level or the first leaf in the subtree respectively
         *
         * @param attributelist the Object Tree
         * @returns {*} the folded attributeList, which is actually a list instead a tree
         * @alias module:CSLEDIT_initData.flatAttributeList
         * @private
         * @author Matthias Cosler
         */
        var flatAttributeList = function (attributelist) {
            $.each(attributelist, function (key, value) {
                attributelist[key] = extractLeaf(value);
            });
            return attributelist;
        };


        /**
         * Unions all the NodeLists (ParsedLists) in NodeListList into one DelimiterList and one AttributeList
         *
         *  - filters the delimiters and the attributes into different lists
         *  - comparison only on the type (variable) or value (delimiter)
         *  - inserts also the standardDelimiters (global variable)
         *  - inserts also the cslNames
         *
         * @param nodeListList the List of Nodelists(parsedLists)
         * @param additionalAttributes, the json with default values for attributes
         * @returns {*} the union of all the delimiters in NodeListList
         * @alias module:CSLEDIT_initData.unionNodes
         * @private
         * @author Matthias Cosler
         */
        var unionNodes = function (nodeListList, additionalAttributes) {
            var delimiterList = [];
            var attributeList = [];
            var freeTextList = [];
            $.each(standardDelimiters, function (index, item) {
                var dummyNode = {"cslIdList" : [] , "type": "delimiter", "styleList": [], "value": "", variableType: undefined};
                dummyNode.value = item;
                delimiterList.push(dummyNode);
            });

            $.each(standardFreeTexts, function (index, item) {
                var dummyNode = {"cslIdList" : [] , "type": "freeText", "styleList": [], "value": "", variableType: undefined};
                dummyNode.value = item;
                freeTextList.push(dummyNode);
            });

            $.each(additionalAttributes, function (key, value) {
                var attribute = {"cslIdList" : [] , "type": "attribute", "styleList": [], "value": "", variableType: ""};
                attribute.variableType =  CSLEDIT_dummyData.replaceWhitechar(value);
                attributeList.push(attribute);
            });

            $.each(nodeListList, function (nodeListIndex, oldNodeList) {
                $.each(oldNodeList, function (nodeIndex, node) {
                    if (node.type === "delimiter" && !nodeListMember(delimiterList, node)) {
                        var newDelimiterNode = {"cslIdList" : [] , "type": "delimiter", "styleList": [], "value": "", variableType: undefined};
                        newDelimiterNode.value = node.value;
                        delimiterList.push(newDelimiterNode);
                    } else if (node.type === "attribute" && !nodeListMember(attributeList, node)){
                        var newAttributeNode = {"cslIdList" : [] , "type": "attribute", "styleList": [], "value": "", variableType: ""};
                        newAttributeNode.variableType = node.variableType;
                        attributeList.push(newAttributeNode);
                    } else if (node.type === "" && !nodeListMember(freeTextList, node)){
                        /*if has no NodeType something went wrong i.e. there is a Type which is not supported. For convenience handle them as if they were freeTexts*/
                        console.log("Error: There is a Node which Type is neither Attribute, nor Delimiter, nor FreeText");
                        var newTextNodeNoType = {"cslIdList" : [] , "type": "freeText", "styleList": [], "value": "", variableType: undefined};
                        newTextNodeNoType.value = node.value;
                        newTextNodeNoType.value += " [" + node.variableType+ "]";
                        freeTextList.push(newTextNodeNoType);
                    } else if (node.type === "freeText" && !nodeListMember(freeTextList, node)){
                        var newTextNode = {"cslIdList" : [] , "type": "freeText", "styleList": [], "value": "", variableType: undefined};
                        newTextNode.value = node.value;
                        freeTextList.push(newTextNode);
                    }
                });
            });
            return [delimiterList,attributeList,freeTextList];
        };

        /**
         * Test Membership of Nodes (from parsedLists) based on {@link module:CSLEDIT_initData.nodeEqual}
         *
         * @param nodeList An JSON Object containing several Objects (Nodes for/from parsedList)
         * @param node An Object (Node from parsedList) which may contained in NodeList
         * @returns {boolean} Whether the Node is in NodeList
         * @alias module:CSLEDIT_initData.nodeListMember
         * @private
         * @author Matthias Cosler
         */
        var nodeListMember = function (nodeList, node) {
            var ret = false;
            $.each(nodeList, function (index, item) {
                if (nodeEqual(item, node)){
                    ret = true;
                    return false;
                }
            });
            return ret;
        };

        /**
         * Equality of Nodes
         *
         * Two nodes are equal if
         *  - their type is equal and (not or) the following
         *      - if variableType not undefined: variableType (a string) is equal
         *      - if variableType is undefined: value (a string) is equal
         *      
         * @param node1
         * @param node2
         * @returns {boolean} true if the nodes are equal
         * @alias module:CSLEDIT_initData.nodeEqual
         * @private
         * @author Matthias Cosler
         */
        var nodeEqual = function (node1, node2) {
            if (node1.type !== node2.type ){
                return false;
            }
            if (node1.variableType === undefined && node2.variableType === undefined){
                if (node1.value === node2.value) {
                    return true;
                }
            } else {
                if (node1.variableType === node2.variableType){
                    return true;
                }
            }
        };

        /**
         * Gets the Data to fill into the CitationType Options (dragnDrop) for one specified Section
         *
         * - gets cslNames from {@link module:CSLEDIT_dummyData.cslNames} and flatten the List using {@link module:CSLEDIT_initData.flatAttributeList}
         * - gets the parsed output from {@link module:CSLEDIT_citationEngine.runCiteprocAndParseOutput}
         * - gather the attributes, delimiter and freeTexts using {@link  module:CSLEDIT_initData.unionNodes}
         * - bundles the information into one compact json
         *
         * @param citationnumber the index of the Section which should be got
         * @returns {{sectionTitle: *, attributeStack: *, citatonFields: ({statusMessage, formattedFurtherCitations, formattedCitations, formattedBibliography}|*)}} an Object containing all the needed Data for one Section
         * @alias module:CSLEDIT_initData.getSectionData
         * @private
         * @author Matthias Cosler
         */
        var getSectionData = function (citationnumber) {
            var cslNames = flatAttributeList(CSLEDIT_dummyData.cslNames()[citationnumber]);
            var parsedList = CSLEDIT_citationEngine.runCiteprocAndParseOutput([[citationnumber]], CSLEDIT_data);

            //extract the Type of citationsource
            var sectionTitle = cslNames.type;
            delete cslNames.type;

            //unions all parsed list and extracts attributes and delimiters in different lists
            var res = unionNodes([parsedList.formattedBibliography, parsedList.formattedFurtherCitations, parsedList.formattedCitations], cslNames);
            var attributes = res[1];
            var delimiterStack = res[0];
            var freeText = res[2];
            return { "sectionTitle" : sectionTitle, "attributeStack" : attributes, "delimiterStack": delimiterStack, "citationFields" : parsedList, "freeTextStack": freeText};

        };


        /**
         * Wraps all sectionData into one Object.
         *
         * @returns {Array} an Array with all the SectionData
         * @alias module:CSLEDIT_initData.getSectionsData
         * @public
         * @author Matthias Cosler
         */
        var getSectionsData = function () {
            var sections = [];
            $.each(CSLEDIT_dummyData.jsonDocumentList, function (index) {
                var res = getSectionData(index);
                sections.push(res);
            });
            return sections;
        };

        /**
         * Get Law Name
         *
         * @param attributeName
         * @param index
         * @return {*}
         * @alias module:CSLEDIT_initData.getLawName
         * @public
         * @author Matthias Cosler
         */
        var getLawName = function (attributeName, index) {
            var flatList = flatAttributeList(CSLEDIT_dummyData.lawNames()[index]);
            attributeName = CSLEDIT_dummyData.replaceWhitechar(attributeName);
            if (flatList.hasOwnProperty(attributeName)){
                return flatList[attributeName];
            } else {
                return attributeName;
            }
        };

        /**
         * Get Zotero Name
         *
         * @param attributeName
         * @param index
         * @return {*}
         * @alias module:CSLEDIT_initData.getZoteroName
         * @public
         * @author Matthias Cosler
         */
        var getZoteroName = function (attributeName, index) {
            var flatList = flatAttributeList(CSLEDIT_dummyData.zoteroNames()[index]);
            attributeName =  CSLEDIT_dummyData.replaceWhitechar(attributeName);
            if (flatList.hasOwnProperty(attributeName)){
                return flatList[attributeName];

            } else {
                return attributeName;
            }        };

        /**
         * Get Description
         *
         * @param attributeName
         * @param index
         * @return {*}
         * @alias module:CSLEDIT_initData.getDescription
         * @public
         * @author Matthias Cosler
         */
        var getDescription = function (attributeName, index) {
            var flatList = flatAttributeList(CSLEDIT_dummyData.descriptions()[index]);
            attributeName =  CSLEDIT_dummyData.replaceWhitechar(attributeName);
            if (flatList.hasOwnProperty(attributeName)){
                return flatList[attributeName];

            } else {
                return attributeName;
            }        };



        return {
            /** @alias module:CSLEDIT_initData.getData
             * @see module:CSLEDIT_initData.getSectionsData */
            getData : getSectionsData,
            getLawName : getLawName,
            getZoteroName : getZoteroName,
            getDescription : getDescription
        };
    });