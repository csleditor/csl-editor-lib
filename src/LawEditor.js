"use strict";

/**
 * A module representing the LawEditor Page
 * @module CSLEDIT_LawEditor
 */
define(
    ['src/controller',
        'src/preProcessor',
        "src/sourceSections",
        'src/LawViewController',
        'src/notificationBar',
        'src/citationEditor',
        'src/Schema',
        'src/schemaOptions',
        'src/CslNode',
        'src/citationEngine',
        'src/options',
        'src/storage',
        'src/dataInstance',
        'src/cslStyles',
        'src/urlUtils',
        'src/debug',
        'jquery.hoverIntent',
        'jquery.layout',
    ],
    function (
        CSLEDIT_controller,
        CSLEDIT_preProcessor,
        CSLEDIT_sourceSections,
        CSLEDIT_LawViewController,
        CSLEDIT_notificationBar,
        CSLEDIT_citationEditor,
        CSLEDIT_Schema,
        CSLEDIT_schemaOptions,
        CSLEDIT_CslNode,
        CSLEDIT_citationEngine,
        CSLEDIT_options,
        CSLEDIT_storage,
        CSLEDIT_data,
        CSLEDIT_cslStyles,
        CSLEDIT_urlUtils,
        debug,
        jquery_hoverIntent,
        jquery_layout
    ) {
        /**
         * Creates a LawEditor Page
         * @class
         * @alias module:CSLEDIT_LawEditor
         * @see module:CSLEDIT_controller
         * @see module:CSLEDIT_preProcessor
         * @see module:CSLEDIT_sourceSections
         * @see module:CSLEDIT_LawViewController
         * @see module:CSLEDIT_notificationBar
         * @see module:CSLEDIT_citationEditor
         * @see module:CSLEDIT_Schema
         * @see module:CSLEDIT_schemaOptions
         * @see module:CSLEDIT_CslNode
         * @see module:CSLEDIT_citationEngine
         * @see module:CSLEDIT_options
         * @see module:CSLEDIT_storage
         * @see module:CSLEDIT_data
         * @see module:CSLEDIT_cslStyles
         * @see module:CSLEDIT_urlUtils
         * @see module:debug
         * @see module:jquery_hoverIntent
         * @see module:jquery_layout
         *
         * @param editorElement
         * @param configurationOptions
         * @author Matthias Cosler
         * @constructor
         */
        var CSLEDIT_LawEditor = function (editorElement, configurationOptions) {

            $(document).ready(function () {
                CSLEDIT_options.setOptions(configurationOptions);
                editorElement = $(editorElement);

                $.ajax({
                    url: CSLEDIT_urlUtils.getResourceUrl("html/lawEditor.html"),
                    success: function (data) {
                        editorElement.html(data);
                        window.CSLEDIT_schema = new CSLEDIT_Schema(CSLEDIT_schemaOptions);
                        CSLEDIT_schema.callWhenReady(init);
                    },
                    error: function (jaXHR, textStatus, errorThrown) {
                        alert("Couldn't fetch page: " + textStatus);
                    },
                    cache: false
                });
            });


            /**
             * creates the view for the LawEditor
             *
             * @see module:CSLEDIT_LawViewController
             *
             */
            var createView = function () {
                var nodeIndex = {index: 0};
                var cslData = CSLEDIT_data.get();

                CSLEDIT_lawViewController.init(cslData,
                    {
                        formatCitations: formatExampleCitations,
                        formatSections: function () {
                            CSLEDIT_sourceSections.reloadContent();
                        }
                    });
            };

            /**
             * formats the example citations by calling {@link module:CSLEDIT_citationEngine.runCiteprocAndDisplayOutput}
             * @alias module:CSLEDIT_LawEditor.formatExampleCitations
             */
            var formatExampleCitations = function () {
                CSLEDIT_citationEngine.runCiteprocAndDisplayOutput(
                    CSLEDIT_data,
                    editorElement.find("#statusMessage"),
                    editorElement.find("#formattedCitations"),
                    editorElement.find("#formattedBibliography"),
                    function () {return undefined;},
                    null,
                    null,
                    editorElement.find("#formattedFurtherCitations"));
            };

            /**
             *
             * @param newURL
             * @alias module:CSLEDIT_LawEditor.reloadPageWithNewStyle
             */
            var reloadPageWithNewStyle = function (newURL) {
                var reloadURL = window.location.href;
                reloadURL = reloadURL.replace(/#/, "");
                reloadURL = reloadURL.replace(/\?.*$/, "");
                window.location.href = reloadURL + "?styleURL=" + newURL;
            };

            /**
             *
             * @param selector
             * @alias module:CSLEDIT_LawEditor.setupDropdownMenuHandler
             */
            var setupDropdownMenuHandler = function (selector) {
                var dropdown = $(selector),
                    loadCsl;

                // Adds the options from the settings into the Style menu
                var styleMenu = CSLEDIT_options.get('styleMenu');
                var styleMenuUl = editorElement.find('#styleMenuUl');
                $.each(styleMenu, function (index, styleOption) {
                    var menuOption = $('<li/>').append($('<a/>')
                        .text(styleOption.label));

                    if (typeof styleOption.name !== 'undefined') {
                        menuOption.attr('id', styleOption.name);
                    }
                    menuOption.click(styleOption.func);
                    styleMenuUl.append(menuOption);
                });

                // If menuNewStyle id exists: will create a new style
                editorElement.find('#menuNewStyle').click(function () {
                    // fetch the URL
                    $.ajax({
                        url: CSLEDIT_urlUtils.getResourceUrl("content/newStyle.csl"),
                        dataType: "text",
                        success: function (cslCode) {
                            debug.log("csl code received: " + cslCode);
                            CSLEDIT_controller.exec('setCslCode', [cslCode]);
                        },
                        error: function () {
                            throw new Error("Couldn't fetch new style");
                        },
                        async: false
                    });
                });

                editorElement.find('#menuUndo').click(function () {
                    if (CSLEDIT_controller.commandHistory.length === 0) {
                        alert("No commands to undo");
                    } else {
                        CSLEDIT_controller.undo();
                    }
                });

                editorElement.find('#menuRedo').click(function () {
                    if (CSLEDIT_controller.undoCommandHistory.length === 0) {
                        alert("No commands to redo");
                    } else {
                        CSLEDIT_controller.redo();
                    }
                });

                // Creates the Help menu if this menu exists. Populates
                // with links
                var helpLinks = CSLEDIT_options.get('helpLinks');
                if (typeof helpLinks !== 'undefined' && helpLinks.length !== 0) {
                    var lawEditorMenu = editorElement.find('#lawEditorMenu');

                    lawEditorMenu.append($('<li/>').attr('id', 'helpMenuMain'));

                    var helpMenuMain = editorElement.find('#helpMenuMain');
                    var helpMenuLink = $('<a/>')
                        .attr('id', 'helpMenu')
                        .text('Help').append($('<span>').attr('class', 'disclosure').html('&#9662;'));

                    helpMenuMain.append(helpMenuLink);

                    helpMenuMain.append($('<ul/>')
                        .attr('id', 'helpMenuUl')
                        .attr('class', 'sub_menu'));

                    var helpMenu = editorElement.find('#helpMenuUl');
                    $.each(helpLinks, function (index, link) {
                        helpMenu.append(($('<li/>').append($('<a/>')
                            .attr('href', link.link)
                            .attr('target', '_blank')
                            .text(link.label))));
                    });
                }

                editorElement.find('#menuEditCitation1').click(function () {
                    CSLEDIT_citationEditor.editCitation(0);
                });

                editorElement.find('#menuEditCitation2').click(function () {
                    CSLEDIT_citationEditor.editCitation(1);
                });

                editorElement.find('#menuEditCitation3').click(function () {
                    CSLEDIT_citationEditor.editCitation(2);
                });
            };

            /**
             * This functions starts the process of initializen the page
             *
             * - calls the Preproceessor {@link module:CSLEDIT_preProcessor}
             * - initializes the notificationBar {@link module:CSLEDIT_notificationBar}
             * - test if the storage has changed meanwhile
             * - registers the views {@link module:CSLEDIT_LawViewController}
             * - loads the contont for the Source Sections {@link module:CSLEDIT_sourceSections}
             *
             * @alias module:CSLEDIT_LawEditor.init
             * @author Matthias Cosler
             */
            var init = function () {
                CSLEDIT_preProcessor.run();

                var showingDataChangePrompt = false;

                CSLEDIT_notificationBar.init(editorElement.find('#notificationBar'));

                // set function which gets called if inconsistencies
                // are found between the localStorage data (shared between tabs) and this session
                // data
                CSLEDIT_storage.onDataInconsistency(function () {
                    showingDataChangePrompt = true;
                    if (confirm("Your style has changed in a different tab.\n" +
                        "Do you want to load the new version into this tab?")) {
                        // reload page
                        window.location.reload();
                    } else {
                        // use existing data
                        CSLEDIT_storage.recreateLocalStorage();
                        showingDataChangePrompt = false;
                    }
                });

                // check consistency of data on window focus
                // to detect changes in different tabs
                debug.log("window length = " + $(window).length);
                $(window).focus(function () {
                    if (!showingDataChangePrompt) {
                        CSLEDIT_data.get();
                    }
                });


                CSLEDIT_data.initPageStyle(function () {
                    var userOnChangeCallback = CSLEDIT_options.get("onChange"), //TODO here
                        citationEditor1,
                        citationEditor2;

                    // TODO: refactor - remove this global
                    window.CSLEDIT_lawViewController = new CSLEDIT_LawViewController(
                        editorElement.find("#titlebar"));

                    CSLEDIT_data.addViewController(CSLEDIT_lawViewController);

                    if (typeof userOnChangeCallback === "function") {
                        CSLEDIT_data.addViewController({
                            styleChanged: function (command) {
                                if (command === "updateFinished") {
                                    userOnChangeCallback();
                                }
                            }
                        });
                    }
                    createView();
                });

                CSLEDIT_sourceSections.loadContent();
                setupDropdownMenuHandler(".dropdown a");
                new CSLEDIT_CslNode("macro", [{key: "name", value: "author-short"}], [], 54);
            };


            /**
             * Called when saving a style. It Checks that the style conforms to repository conventions and prompts the user to change it if it doesn't
             * @returns {boolean} Returns true to continue saving, false to cancel
             */
            var conformStyleToRepoConventions = function () {
                var generatedStyleId,
                    links,
                    selfLinkNode,
                    selfLink,
                    styleName = getStyleName(),
                    cancel = false;

                // check that the styleId and rel self link matches the schema conventions
                generatedStyleId = CSLEDIT_cslStyles.generateStyleId(getStyleName());
                links = CSLEDIT_data.getNodesFromPath("style/info/link");
                $.each(links, function (i, link) {
                    link = new CSLEDIT_CslNode(link);

                    if (link.getAttr("rel") === "self") {
                        selfLinkNode = link;
                        selfLink = link.getAttr("href");
                    }
                });

                debug.log("generatedStyleId = " + generatedStyleId);
                $.each(CSLEDIT_cslStyles.styles().styleTitleFromId, function (id, name) {
                    if (id === generatedStyleId || name === styleName) {
                        if (!confirm('The style title matches one that already exists.\n\n' +
                            'You should change it to avoid problems using this style ' +
                            'in your reference manager.\n\n' +
                            'Do you want to save anyway?')) {
                            cancel = true;
                            return false;
                        }
                    }
                });

                if (cancel) {
                    return false;
                }

                if (selfLink !== generatedStyleId || getStyleId() !== generatedStyleId) {
                    if (confirm('Change style ID and "self" link to the following?\n\n' +
                        generatedStyleId + "\n\n(the CSL styles repository convention)")) {
                        setStyleId(generatedStyleId);
                        if (typeof(selfLinkNode) !== "undefined") {
                            selfLinkNode.setAttr("href", generatedStyleId);
                            CSLEDIT_controller.exec("amendNode", [selfLinkNode.cslId, selfLinkNode]);
                        } else {
                            CSLEDIT_controller.exec("addNode", [CSLEDIT_data.getNodesFromPath("style/info")[0].cslId, "last",
                                new CSLEDIT_CslNode("link", [
                                    {key: "rel", value: "self", enabled: true},
                                    {key: "href", value: generatedStyleId, enabled: true}
                                ])]);
                        }
                    }
                }
                return true;
            };

            /**
             * Sets a new CSL style from the given cslCode string
             * @param cslCode
             * @returns {*}
             * @alias module:CSLEDIT_lawEditor.setCslCode
             */
            var setCslCode = function (cslCode) {
                return CSLEDIT_controller.exec('setCslCode', [cslCode]);
            };

            /**
             * @returns {*}  the current CSL style code as a string
             * @alias module:CSLEDIT_lawEditor.getCslCode
             */
            var getCslCode = function () {
                return CSLEDIT_data.getCslCode();
            };

            /**
             * @returns {*} the current style name
             * @alias module:CSLEDIT_lawEditor.getStyleName
             */
            var getStyleName = function () {
                var styleNameNode = CSLEDIT_data.getNodesFromPath('style/info/title')[0];
                return styleNameNode.textValue;
            };

            /**
             * @returns {*} the current style ID
             * @alias module:CSLEDIT_lawEditor.getStyleId
             */
            var getStyleId = function () {
                var styleIdNode = CSLEDIT_data.getNodesFromPath('style/info/id')[0];
                return styleIdNode.textValue;
            };

            /**
             * Sets the ID for the current style
             *
             * @param styleId
             * @alias module:CSLEDIT_lawEditor.setStyleId
             */
            var setStyleId = function (styleId) {
                var styleIdNode = CSLEDIT_data.getNodesFromPath('style/info/id')[0];
                styleIdNode.textValue = styleId;
                CSLEDIT_controller.exec('amendNode', [styleIdNode.cslId, styleIdNode]);
            };


            return {
                setCslCode: setCslCode,
                getCslCode: getCslCode,
                getStyleName: getStyleName,
                getStyleId: getStyleId,
                conformStyleToRepoConventions: conformStyleToRepoConventions
            };
        };

        return CSLEDIT_LawEditor;
    });



