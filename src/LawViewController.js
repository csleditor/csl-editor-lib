"use strict";

/**
 * @module CSLEDIT_LawViewController
 * @author Matthias Cosler
 *
 */
define(
    [	'src/Titlebar',
        'src/notificationBar',
        'src/CslNode',
        'src/Iterator',
        'src/dataInstance',
        'src/debug'
    ],
    function (
        CSLEDIT_Titlebar,
        CSLEDIT_notificationBar,
        CSLEDIT_CslNode,
        CSLEDIT_Iterator,
        CSLEDIT_data,
        debug
    ) {
        /**
         * Creates a ViewController responsible for adding and maintaining the content in all the given jQuery elements
         *
         * An CSLEDIT_ViewController instance ensures that all the views are notified whenever one of the following functions is called:
         *  - addNode
         *  - deleteNode
         *  - amendNode
         *  - newStyle
         *
         * @class
         * @alias module:CSLEDIT_LawViewController
         * @see module:CSLEDIT_Titlebar
         * @see module:CSLEDIT_notificationBar
         * @see module:CSLEDIT_CslNode
         * @see module:CSLEDIT_Iterator
         * @see module:CSLEDIT_data
         *
         *
         * @param titlebarElement
         * @returns {{init: init, addNode: addNode, deleteNode: deleteNode, updateFinished: updateFinished, styleChanged: styleChanged, newStyle: newStyle, amendNode: amendNode}}
         * @constructor
         * @author Matthias Cosler
         */
        var CSLEDIT_LawViewController = function (
            titlebarElement) {

            var views = [],
                callbacks,
                recentlyEditedMacro = -1;


            /**
             * Sets up all the views (at this point only titlebar
             * @param cslData
             * @param _callbacks
             * @alias module:CSLEDIT_LawViewController.init
             * @author Matthias Cosler
             * @public
             */
            var init = function (cslData, _callbacks) {

                views = [];

                views.push(new CSLEDIT_Titlebar(titlebarElement));

                callbacks = _callbacks;

                callbacks.formatCitations();
            };

            /**
             * Checks if a macro with multple occurences is edited and show notification
             *
             * @param id
             * @alias module:CSLEDIT_LawViewController.macroEditNotification
             * @private
             */
            var macroEditNotification = function (id) {
                var nodeStack = CSLEDIT_data.getNodeStack(id),
                    node,
                    iter,
                    next,
                    macroName,
                    timesUsed;

                while (nodeStack.length > 0) {
                    node = nodeStack.pop();
                    if (node.name === "macro" && recentlyEditedMacro !== node.cslId) {
                        macroName = new CSLEDIT_CslNode(node).getAttr("name");
                        if (macroName === "") {
                            return;
                        }

                        // check how many places this macro is used
                        iter = new CSLEDIT_Iterator(CSLEDIT_data.get());
                        timesUsed = 0;

                        while (iter.hasNext()) {
                            next = new CSLEDIT_CslNode(iter.next());

                            if (next.name === "text" && next.getAttr("macro") === macroName) {
                                timesUsed++;

                                if (timesUsed > 1) {
                                    recentlyEditedMacro = node.cslId;
                                    CSLEDIT_notificationBar.showMessage(
                                        'You just edited a macro which is used in multiple places');
                                }
                            }
                        }
                    }
                }
            };

            /**
             * Responds on an addNode event
             *
             * @param id
             * @param position
             * @param newNode
             * @param nodesAdded
             * @alias module:CSLEDIT_LawViewController.addNode
             * @public
             */
            var addNode = function (id, position, newNode, nodesAdded) {
                macroEditNotification(id);
                $.each(views, function (i, view) {
                    if ("addNode" in view) {
                        view.addNode(id, position, newNode, nodesAdded);
                    }
                });
            };

            /**
             * Responds to a deleteNode event
             *
             * @param id
             * @param nodesDeleted
             * @alias module:CSLEDIT_LawViewController.deleteNode
             * @todo not implemented
             * @public
             */
            var deleteNode = function (id, nodesDeleted) {
                //TODO
            };

            /**
             * Responds to an amendNode event
             *
             * @param id
             * @param amendedNode
             * @public
             * @alias module:CSLEDIT_LawViewController.amendNode
             */
            var amendNode = function (id, amendedNode) {
                macroEditNotification(id);
                $.each(views, function (i, view) {
                    if ("amendNode" in view) {
                        view.amendNode(id, amendedNode);
                    }
                });
            };

            /**
             * Is called when the updated is finished
             * @alias module:CSLEDIT_LawViewController.updateFinished
             * @public
             */
            var updateFinished = function () {
                callbacks.formatCitations();
            };

            /**
             * Responds to the newStyle event,
             * Uses the nuclear option and re-builds everything
             * @alias module:CSLEDIT_LawViewController.newStyle
             * @public
             */
            var newStyle = function () {
               init(CSLEDIT_data.get(), callbacks);
               callbacks.formatSections();
            };

            /**
             * Will execute the CSLEDIT_ViewController member function with name 'command' and pass it the given arguments
             *
             * @param command
             * @param args
             * @alias module:CSLEDIT_LawViewController.styleChanged
             * @public
             */
            var styleChanged = function (command, args) {
                args = args || [];
                debug.log("executing view update: " + command + "(" + args.join(", ") + ")");
                this[command].apply(null, args);
            };

            // public:
            return {
                init : init,

                addNode : addNode,
                deleteNode : deleteNode,
                amendNode : amendNode,
                newStyle : newStyle,
                updateFinished : updateFinished,

                styleChanged : styleChanged
            };
        };

        return CSLEDIT_LawViewController;
    });

