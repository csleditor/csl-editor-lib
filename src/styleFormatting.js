"use strict";

/**
 * Supports the LawEditor with all functions necessary to format a field and to handle the Toolbar
 *
 * @module CSLEDIT_styleFormatting
 * @see module:CSLEDIT_AttributesToolbar
 * @see module:CSLEDIT_data
 * @see module:CSLEDIT_controller
 * @author Matthias Cosler
 */
define(
    [   'src/AttributesToolbar',
        'src/dataInstance',
        'src/controller'
    ],
    function (
        CSLEDIT_AttributesToolbar,
        CSLEDIT_data,
        CSLEDIT_controller
    ) {

        /**
         * Stores the global options for formatting
         *
         * @alias module:CSLEDIT_styleFormatting.formatOptions
         * @public
         * @example
         *      [{
         *          style : "bold", //  data-style attr.
         *          title: "Bold", // title attr
         *          html : "<b>B</b>", // innerHTML of button, use to format button caption
         *          tag : "B", // HTML tag, as citeproc-js may output this format
         *          css : [["font-weight","bold","normal"]], // options to format css, csl: option to format csl [name, format, default]
         *          csl: ["font-weight","bold","normal"], option to format csl [name, format, default]
         *          group : 1 //a number which represents the group with the same default case
         *      }, ...]
         *
         * @see {@link https://docs.citationstyles.org/en/1.0.1/specification.html#formatting}
         * @see {@link https://docs.citationstyles.org/en/1.0.1/specification.html#text-case}
         * @type {array}
         * @author Matthias Cosler
         */
        var formatOptions = [
            {style : "bold", title: "Bold", html : "<b>B</b>", tag : "B", css : [["font-weight","bold","normal"]], csl: ["font-weight","bold","normal"], group : 1},
            {style : "light", title: "Light", html : "<b>L</b>", tag : "DIV", css : [["font-weight","lighter","normal"]], csl: ["font-weight","light","normal"], group : 1},
            {style : "oblique", title: "Oblique", html : "<div style=\"font-style: oblique;\">O</div>", tag : "DIV", css : [["font-style","oblique","normal"]], csl: ["font-style","oblique","normal"], group : 2},
            {style : "italic", title: "Italic", html : "<i>I</i>", tag : "I", css : [["font-style","italic","normal"]], csl: ["font-style","italic","normal"], group : 2},
            {style : "underline", title: "Underline", html : "<u>U</u>", tag : "U", css : [["text-decoration","underline","none"]], csl: ["text-decoration","underline","none"], group : 3},
            {style : "superscript", title: "Superscript", html : "x<sup>s</sup>", tag : "SUP", css : [["vertical-align","super","baseline"],["font-size","smaller","larger"]], csl: ["vertical-align","sup","baseline"], group : 4},
            {style : "subscript", title: "Subscript", html : "x<sub>s</sub>", tag : "SUB", css : [["vertical-align","sub","baseline"],["font-size","smaller","larger"]], csl: ["vertical-align","sub","baseline"], group : 4},
            {style : "small-caps", title: "Small Caps", html : "<div style=\"font-variant: small-caps;\">Small Caps</div>", tag : "DIV", css : [["font-variant","small-caps","normal"]], csl: ["font-variant","small-caps","normal"], group : 5},
            {style : "lowercase", title: "Lowercase", html : "<div style=\"text-transform: lowercase;\">Lower</div>", tag : "DIV", css : [["text-transform","lowercase","none"]], csl: ["text-case","lowercase"], group : 6},
            {style : "uppercase", title: "Uppercase", html : "<div style=\"text-transform: uppercase;\">Upper</div>", tag : "DIV", css : [["text-transform","uppercase","none"]], csl: ["text-case","uppercase"], group : 6},
            {style : "capitalize-all", title: "Capitalize the first letter in each word", html : "<div style=\"text-transform: capitalize;\">caps</div>", tag : "DIV", css : [["text-transform","capitalize","none"]], csl: ["text-case","capitalize-first"], group : 6}
        ];

        /**
         * Stores the standard Values for formatting
         *
         * @alias module:CSLEDIT_styleFormatting.standardFormatOptions
         * @public
         * @example
         *      [{option: "bold", enabled : false}, ...]
         *
         * @type {array}
         * @author Matthias Cosler
         */
        var standardFormatOptions = [
            {option: "bold", enabled : false},
            {option: "italic", enabled : false},
            {option: "underline", enabled : false},
            {option: "superscript", enabled : false},
            {option: "subscript", enabled : false},
            {option: "small-caps", enabled : false},
            {option: "lowercase", enabled : false},
            {option: "uppercase", enabled : false},
            {option: "capitalize-all", enabled : false}
        ];

        /**
         * Is triggered whenever a button of the toolbar is clicked.
         *
         *  - if the button is selected (which means it wasn't before)
         *      - call {@link module:CSLEDIT_styleFormatting.applyFormat}
         *      - call {@link module:CSLEDIT_styleFormatting.addFormatToNode}
         *  - if the button is not selected (which means it was before)
         *      - call {@link module:CSLEDIT_styleFormatting.applyFormat} with option "remove"
         *      - call {@link module:CSLEDIT_styleFormatting.addFormatToNode} with option "remove"
         *
         * @alias module:CSLEDIT_styleFormatting.toolbarChanged
         * @private
         * @param {*|jQuery} $button the button which triggered the event
         * @author Matthias Cosler
         */
        var toolbarChanged = function ($button) {

            var option = $button.attr("data-style");
            var json = JSON.parse($button.parent().parent().attr("obj"));
            var cslID = json.currentID;

            if ($button.hasClass("selected")) {
                applyFormat($button.parent().parent(), option);
                addFormatToNode(cslID, option);

            } else {
                applyFormat($button.parent().parent(), option, true);
                addFormatToNode(cslID, option, true);
            }
        };

        /**
         * Clones the toolbar
         *
         *  - extract the options (buttons and enabled buttons) from the old toolbar
         *  - calling {@link module:CSLEDIT_styleFormatting.addToolbar} with the extracted options
         *
         * @alias module:CSLEDIT_styleFormatting.cloneToolbar
         * @public
         * @param {object} toolbar the old toolbar
         * @param {*|jQuery} $element the element in which the cloned toolbar should be anchored
         * @author Matthias Cosler
         */
        function cloneToolbar(toolbar, $element) {
            var buttons = $(toolbar).children("a");
            var options = [];
            $.each(buttons, function (index, button) {
                button = $(button);
                if (button.hasClass("selected")) {
                    options.push({option : button.attr("data-style"), enabled: true});
                } else {
                    options.push({option : button.attr("data-style"), enabled: false});
                }
            });
            addToolbar($element, options);
        }

        /**
         * Adds the Toolbar to a containerElement
         *
         *  - if o options passed, take the {@link module:CSLEDIT_styleFormatting.standardFormatOptions}
         *  - make new {@link module:CSLEDIT_AttributesToolbar} inside the container element $container
         *
         * @alias module:CSLEDIT_styleFormatting.addToolbar
         * @public
         * @param {*|jQuery} $element the jquery-element the toolbar will be added to
         * @param {array} options a list of options, which should be displayed. every option can be enabled or disabled.
         * @example <caption> Example for options </caption>
         *      [{option: "bold", enabled : true}, ...]
         * @author Matthias Cosler
         */
        var addToolbar = function ($element, options) {
            //on every call for addToolbar, the correct options need to passed or the standardOptions will be used

            if (options === undefined) {
                options = standardFormatOptions;
            }

            new CSLEDIT_AttributesToolbar($element, options, formatOptions, toolbarChanged);
        };

        /**
         * Applies a formatting on an html element and uses the format name to specify which format to use
         *
         *  - search for the css code with the given option name (in {@link module:CSLEDIT_styleFormatting.formatOptions })
         *  - find the elemnt on which the css property will be applied
         *      - for input fields it is the field directly, otherwise it is the div containing the text
         *  - if remove is not set, add new css property
         *  - if remove is set, remove the css property
         *
         * @alias module:CSLEDIT_styleFormatting.applyFormat
         * @private
         * @param {*|jQuery} $element the element the formatiing should be apllied on
         * @param {string} option the format which should be used
         * @param {bool} remove an optional flag, which sets that the option is to be unset
         * @throws Error if no css code with the given option found
         * @author Matthias Cosler
         */
        var applyFormat = function ($element, option, remove) {
            var found = true;
            var css = "";
            if (remove === undefined){
                remove = false;
            }

            $.each(formatOptions, function (searchIndex, searchItem) {
                if (searchItem.style === option){
                    found = true;
                    css = searchItem.css;
                    return false;
                }
            });
            if (found === false){
                throw new Error("style \"" + option + "\" has no template in toolbar");
            }

            var $elementToChange;
            if ($($element.children()[0]).find("input")[0] === undefined){
                $elementToChange = $($element.children()[0]);
            } else {
                $elementToChange = $($($element.children()[0]).find("input")[0]);
            }
            if (!remove) {
                $.each(css, function (index, item) {
                    $elementToChange.css(item[0], item[1]);
                });
            } else {
                $.each(css, function (index, item) {
                    $elementToChange.css(item[0], "");
                });
            }

            //TODO also change json
        };

        /**
         * Performs a format to a CSL Node
         *
         *  - make sure that cslId exists
         *  - search for the csl code with the given option name (in {@link module:CSLEDIT_styleFormatting.formatOptions })
         *  - find the Node to the given CSLId
         *  - remove the csl property from this node (this also will remove eventual default options)
         *  - if remove is active
         *      - set the default value for this property, in case the old format was defined in an ancestor node (but not for text-case)
         *  - if remove is not active
         *      - set the new property
         *  - using the {@link module:CSLEDIT_controller} to apply changes to the csl tree
         *
         * @alias module:CSLEDIT_styleFormatting.addFormatToNode
         * @private
         * @param {number} cslId the node which is to be changed
         * @param {string} option the format which will be applied
         * @param {bool} remove an optional flag, which sets that the option is to be unset
         * @throws Error if no csl code with the given option found
         * @author Matthias Cosler
         */
        var addFormatToNode = function (cslId, option, remove) {
            if (cslId !== undefined) {
                var found = true;
                var csl = {};
                if (remove === undefined) {
                    remove = false;
                }

                $.each(formatOptions, function (searchIndex, searchItem) {
                    if (searchItem.style === option) {
                        found = true;
                        csl = searchItem.csl;
                        return false;
                    }
                });
                if (found === false) {
                    throw new Error("style \"" + option + "\" has no template in toolbar");
                }
                var oldNode = CSLEDIT_data.getNode(cslId);
                var newAttribute = {key: csl[0], value: "", enabled: true};


                //remove the attribute form the innerst node if possible this will remove the default value and ppssiblbe other values
                var indexToDelete;
                $.each(oldNode.attributes, function (index, item) {
                    if (item.key === csl[0]) {
                        indexToDelete = index;
                    }
                });
                if (indexToDelete !== undefined) {
                    oldNode.attributes.splice(indexToDelete, 1);
                }

                if (remove) {
                    if (csl[0] !== "text-case") {
                        //if it is not text-transform also add the default value because, the format could have been defined in a ancestor node
                        newAttribute.value = csl[2];
                        oldNode.attributes.push(newAttribute);
                    }
                } else if (!remove) {
                    newAttribute.value = csl[1];
                    oldNode.attributes.push(newAttribute);
                }
                CSLEDIT_controller.exec("amendNode", [cslId, oldNode]);
            }
        };

        /**
         * Returns the first format option which has either a corresponding tag or a corresponding css
         *
         *  - iterate through {@link module:CSLEDIT_styleFormatting.formatOptions}
         *      - on each use UpperCase and no whitespaces to standardise
         *      - test if css code is tagcss
         *      - test if tag code is tagcss
         *
         * @alias module:CSLEDIT_styleFormatting.getNameforFormat
         * @private
         * @param {string} tagcss
         * @throws Error in case no style could be found with this tagcss
         * @returns {*} the name of the found style (undefined if no style !active! with this tagcss)
         * @author Matthias Cosler
         */
        var getNameforFormat = function (tagcss){
            var ret;
            var set = false;
            $.each(formatOptions, function (index, item) {
                var css = item.css[0][0] + ":" + item.css[0][1] + ";";
                var css_def = item.css[0][0] + ":" + item.css[0][2] + ";";
                var css_def2 = item.css[0][2];

                tagcss.replace(/\s/g, "");
                css.replace(/\s/g, "");
                css_def.replace(/\s/g, "");
                if (tagcss.toUpperCase() === item.tag.toUpperCase()){
                    ret = item.style;
                    set = true;
                    return false;
                } else if (css.toUpperCase() === tagcss.toUpperCase()){
                    ret = item.style;
                    set = true;
                    return false;
                } else if (css_def.toUpperCase() === tagcss.toUpperCase() || css_def2.toUpperCase() === tagcss.toUpperCase()) {
                    ret = undefined;
                    set = true;
                    return false;
                }
            });
            if (!set){
                throw new Error("to the HTML or CSS Code \"" + tagcss + "\" were no corresponding option found");
            }
            return ret;
        };

        /**
         * Extracts the enabled options for a toolbar on base of the json object which is stored in the html code
         *
         * - iterate trough styleList in item
         *      - find option name for each tag or css using {@link module:CSLEDIT_styleFormatting.getNameforFormat}
         *      - store it in a list of found options
         * - for all {@link module:CSLEDIT_styleFormatting.standardFormatOptions} enable the option if it is in the list of found options
         *
         * @alias module:CSLEDIT_styleFormatting.getOptionsFromItem
         * @public
         * @param {object} item the json object, which is stroed in each element
         * @example <caption> Example for item </caption>
         *     {
         *       "styleList": ["I"],
         *       "cslIdList": [["1213","group"],["106","macro"],["795","macro"],["796","group"],["798","date"]],
         *       "value": "Datum Veröffentlichung",
         *       "type": "attribute",
         *       "variableType": "issued",
         *       "currentID": 798
         *     }
         * @example <caption> Example for return object </caption>
         *      [{option: "bold", enabled : true},
         *      {option: "underline", enabled : true}, ... ]
         * @returns {Array} the options with enabled flag for all found options
         * @author Matthias Cosler
         */
        var getOptionsFromItem = function (item) {

            //find the options which are enabled at the beginning
            var formatsUsed = [];
            $.each(item.styleList, function (index, item) {
                var format = getNameforFormat(item);
                if (format !== undefined) {
                    formatsUsed.push(format);
                }
            });

            //enable the options which are used from the beginning
            var options = [];
            $.each(standardFormatOptions, function (index, item) {
                var found = false;
                $.each(formatsUsed, function (indexUsed, itemUsed) {
                    if (itemUsed===item.option){
                        options.push({option : item.option, enabled : true});
                        found = true;
                        return false;
                    }
                });
                if (!found){
                    options.push({option : item.option, enabled : false});
                }
            });
            return options;
        };

        /**
         * applies all Formats based on options to the toolbar which his in $elementcontainer
         *
         * for each option call {@link module:CSLEDIT_styleFormatting.applyFormat}
         *
         * @alias module:CSLEDIT_styleFormatting.applyFormats
         * @private
         * @param $elementcontainer the container for the toolbar
         * @param options the set of options which are to be applied
         * @public
         * @author Matthias Cosler
         */
        var applyFormats = function ($elementcontainer, options) {
            //apply the options which were enabled from the beginning
            $.each(options, function (index, item) {
                if (item.enabled) {
                    applyFormat($elementcontainer, item.option);
                }
            });
        };


        return {
            addToolbar : addToolbar,
            cloneToolbar : cloneToolbar,
            applyFormats : applyFormats,
            getOptionsFromItem : getOptionsFromItem,
            formatOptions : formatOptions,
            standardFormatOptions : standardFormatOptions
        };
    });



