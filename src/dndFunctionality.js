"use strict";

/**
 *
 * Handles i.e initializes the Drag and Drop Functionality inside the SourceSections
 *
 * @module CSLEDIT_dndFunctionality
 * @see module:CSLEDIT_sourceSections
 */
define(
    [
        'src/sourceSections'
    ],
    function (
        CSLEDIT_sourceSections
    ) {

        /**
         * Tests if the Element before or after the destination place is also a delimiter (but not if destination ist DelimitersDND)
         * @param event
         * @param ui
         * @returns {*|boolean}
         * @alias module:CSLEDIT_dndFunctionality.siblingIsDelimiter
         * @author Matthias Cosler
         * @private
         */
        function siblingIsDelimiter(event, ui) {
            var isDelimiter = (ui.item.children().is(".DelimiterContent"));
            var nextIsDelimiter;
            var prevIsDelimiter;
            if (ui.item.next().length) {
                nextIsDelimiter = (ui.item.next().children().is(".DelimiterContent"));
            } else nextIsDelimiter = false;
            if (ui.item.prev().length) {
                prevIsDelimiter = (ui.item.prev().children().is(".DelimiterContent"));
            } else prevIsDelimiter = false;
            var targetIsDelimiter = ui.item.parent().is(".DelimitersDND");
            return (isDelimiter && (prevIsDelimiter || nextIsDelimiter) && (!targetIsDelimiter));
        }

        /**
         * Tests if the Element was between two delimiters but not if it is in DelimiterSource
         * @param before
         * @returns {boolean}
         * @alias module:CSLEDIT_dndFunctionality.siblingsWereDelimiters
         * @author Matthias Cosler
         * @private
         */
        function siblingsWereDelimiters(before) {
            var nextWasDelimiter;
            var prevWasDelimiter;
            var parentDelimters;
            if (before.length) {
                prevWasDelimiter = (before.children().is(".DelimiterContent"));
            } else prevWasDelimiter = false;
            if (prevWasDelimiter && before.next().length) {
                nextWasDelimiter = (before.next().children().is(".DelimiterContent"));
            } else nextWasDelimiter = false;
            if (prevWasDelimiter && before.parent().length) {
                parentDelimters = (before.parent().is(".DelimitersDND"));
            } else parentDelimters = false;

            return (prevWasDelimiter && nextWasDelimiter && !parentDelimters);
            /*prevWasDelimiter is redundant here but for better understanding it is written*/
        }

        /**
         * Highlights all Direct Siblings, which are Delimiters
         * @param objectrelated Object of the Element which should be in relation
         * @alias module:CSLEDIT_dndFunctionality.highlightDirectDelimiterSiblings
         * @author Matthias Cosler
         * @private
         */
        function highlightDirectDelimiterSiblings(objectrelated) {
            if (objectrelated.next().length) {
                if (objectrelated.next().children().is(".DelimiterContent")) {
                    objectrelated.next().effect('highlight', {color: "#ec8e0c"}, 1000);
                }
            }
            if (objectrelated.prev().length) {
                if (objectrelated.prev().children().is(".DelimiterContent")) {
                    objectrelated.prev().effect('highlight', {color: "#ec8e0c"}, 1000);
                }
            }
        }


        /**
         * Configures all necessary things for the drag and drop functionality
         *
         * Please have a look at the Source Code.
         * @alias module:CSLEDIT_dndFunctionality.initDndFunctions
         * @author Matthias Cosler
         * @public
         */
        var initDndFunctions = function (initClones) {

            /*Jquery UI Layout Plugin initialization*/
            $("#mainContainer").layout({
                north__size: 300,
                livePaneResizing: true,
                center__onresize: $.layout.callbacks.resizePaneAccordions
            });

            /*JQuery UI Accordion initialization*/
            $("#OptionContent").accordion({heightStyle: "panel"});
            /*TODO distance between real beginning and end of header*/


            /* JQuery UI Sortable DragnDrop initialization with the following custom extensions
            * - Placeholder
            * - drop Animation
            * - allows DragnDrop with not-deleting from Source but from Target by dragging it into different Lists (Inspired by https://forum.jquery.com/topic/jquery-sortable-clone#14737000005252131)
            * - allows Drop only in the related Source or in a Target
            * - not allows moves such that two Delimiters in Target were next to each other*/
            var clone, before, parent;

            /* Initializes the Targets
            * - Element can be dragged away from here (see start)
            * - Delimiter can't be dropped near existing (see receive, stop)
            * - Element can't be removed between two delimiters (see receive, stop)
            * - Dropped Elements from Sources are cloned by dropping (see receive) */
            $('.AttributesDNDTarget').sortable({
                connectWith: $(".AttributesDND, .DelimitersDND, .FreeTextsDND"),
                helper: "clone",
                revert: true,
                placeholder: "ui-sortable-placeholder",
                cursor: "move",
                start: function (event, ui) {
                    $(ui.item).show();
                    clone = $(ui.item).clone();
                    before = $(ui.item).prev();
                    parent = $(ui.item).parent();
                },
                receive: function (event, ui) { //only when dropped from one to another!
                    var siblingIsDel = siblingIsDelimiter(event, ui);
                    var siblingsWereDel = siblingsWereDelimiters(before);
                    if (parent.is(".AttributesDNDSource") || parent.is(".DelimitersDND") || parent.is(".FreeTextsDND") || siblingIsDel || siblingsWereDel) {
                        if (before.length) before.after(clone);
                        else parent.prepend(clone);
                        initClones(clone, $(ui.item));
                    }
                    if (siblingIsDel || siblingsWereDel) {
                        var objectrelated;
                        if (siblingIsDel) objectrelated = ui.item;
                        if (siblingsWereDel) objectrelated = clone;
                        highlightDirectDelimiterSiblings(objectrelated);
                        ui.item.remove();
                        initClones(clone, $(ui.item));
                    }
                },
                stop: function (event, ui) { //only when dropped in the same list
                    var siblingIsDel = siblingIsDelimiter(event, ui);
                    var siblingsWereDel = siblingsWereDelimiters(before);
                    if (siblingIsDel || siblingsWereDel) {
                        if (before.length) before.after(clone);
                        else parent.prepend(clone);
                        var objectrelated;
                        if (siblingIsDel) objectrelated = ui.item;
                        if (siblingsWereDel) objectrelated = clone;
                        highlightDirectDelimiterSiblings(objectrelated);
                        ui.item.remove();
                    }
                }
            }).disableSelection();

            /*Initializes the Attribute Source
            * Elements can be dragged from here (to Targets) (see start)
            * Only Attributes can be dropped here (see receive) */
            $('.AttributesDNDSource').sortable({
                connectWith: $(".AttributesDND"),
                helper: "clone",
                revert: true,
                placeholder: "ui-sortable-placeholder",
                cursor: "move",
                start: function (event, ui) {
                    $(ui.item).show();
                    clone = $(ui.item).clone();
                    before = $(ui.item).prev();
                    parent = $(ui.item).parent();
                },
                receive: function (event, ui) {
                    if (!ui.item.children().is(".AttributeContent")) {
                        if (before.length) before.after(clone);
                        else parent.prepend(clone);
                        //event.preventDefault();
                        //event.stopPropagation();
                        ui.item.remove();
                        CSLEDIT_sourceSections.initClones(clone, $(ui.item));
                    }
                }
            }).disableSelection();

            /*Initializes the Delimiter Source
            * Elements can be dragged from here (to Targets) (see start)
            * Only Delimiters can be dropped here (see receive)*/
            $('.DelimitersDND').sortable({
                connectWith: $(".AttributesDNDTarget, .DelimitersDND"),
                helper: "clone",
                revert: true,
                placeholder: "ui-sortable-placeholder",
                cursor: "move",
                start: function (event, ui) {
                    $(ui.item).show();
                    clone = $(ui.item).clone();
                    before = $(ui.item).prev();
                    parent = $(ui.item).parent();
                },
                receive: function (event, ui) {
                    if (!ui.item.children().is(".DelimiterContent")) {
                        if (before.length) before.after(clone);
                        else parent.prepend(clone);
                        ui.item.remove();
                        CSLEDIT_sourceSections.initClones(clone, $(ui.item));
                    }
                }
            }).disableSelection();

            /*Initializes the FreeTexts Source
            * Elements can be dragged from here (to Targets) (see start)
            * Only FreeTexts can be dropped here (see receive)*/
            $('.FreeTextsDND').sortable({
                connectWith: $(".AttributesDNDTarget, .FreeTextsDND"),
                helper: "clone",
                revert: true,
                placeholder: "ui-sortable-placeholder",
                cursor: "move",
                start: function (event, ui) {
                    $(ui.item).show();
                    clone = $(ui.item).clone();
                    before = $(ui.item).prev();
                    parent = $(ui.item).parent();
                },
                receive: function (event, ui) {
                    if (!ui.item.children().is(".FreeTextContent")) {
                        if (before.length) before.after(clone);
                        else parent.prepend(clone);
                        ui.item.remove();
                        CSLEDIT_sourceSections.initClones(clone, $(ui.item));
                    }
                }
            }).disableSelection();
        };
    return {
        initDndFunctions : initDndFunctions
    };
});



